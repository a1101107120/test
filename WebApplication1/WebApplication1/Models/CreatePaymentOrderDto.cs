﻿using System.ComponentModel.DataAnnotations;
using WebApplication1.Extension;

namespace WebApplication1.Models
{
    public class CreatePaymentOrderDto
    {
        public void GivenNullParameter()
        {
            if (string.IsNullOrWhiteSpace(OrderNo))
            {
                OrderNo = BaseExtensions.GetRandomOrderNo();
            }

            if (string.IsNullOrWhiteSpace(LoginName))
            {
                LoginName = "Admin";
            }
            if (string.IsNullOrWhiteSpace(PayName))
            {
                PayName = "Test";
            }
            if (string.IsNullOrWhiteSpace(RequestTime))
            {
                RequestTime = BaseExtensions.GetTimeRequest().ToString();
            }

            if (string.IsNullOrWhiteSpace(ClientIp))
            {
                ClientIp = "127.0.0.1";
            }
        }

       

        [Required(ErrorMessage = "MerchantId必填")]
        public string MerchantId { get; set; }
        [Required(ErrorMessage = "MerchantPushDataId必填")]
        public string MerchantPushDataId { get; set; }
        [Required(ErrorMessage = "MerchantPayInfoId必填")]
        public string MerchantPayInfoId { get; set; }
        public string OrderNo { get; set; }

        public string LoginName { get; set; }
        [Required(ErrorMessage = "金額必填")]
        public decimal Amount { get; set; }
        public string PayName { get; set; }
        public string RequestTime { get; set; }
        public string ClientIp { get; set; }

        [Required(ErrorMessage = "密鑰必填")]
        public string Sign { get; set; }

        [Required(ErrorMessage = "本地IP必填")]
        public string LocalIp { get; set; }
    }
}