﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using WebApplication1.Controllers;

namespace WebApplication1.Models
{
    public class CrawlerAmOddsOnlineData
    {
        [JsonProperty("i")]
        public List<dynamic> i { get; set; }
        [JsonProperty("eg")]
        public AmOddsEgInnerData eg { get; set; }
        [JsonProperty("v")]
        public int v { get; set; }
        [JsonProperty("fb")]
        public List<FbData> fb { get; set; }
        [JsonProperty("ot")]
        public int ot { get; set; }

    }
    public class AmOddsEgInnerData
    {
        public EgsInnerC c { get; set; }
        public List<AmOddsEsData> es { get; set; }
    }
    public class AmOddsEsData
    {
        public bool dbg { get; set; }
        public int egid { get; set; }
        public string g { get; set; }
        public List<string> i { get; set; }
        public bool ibs { get; set; }
        public bool ibsc { get; set; }
        public int k { get; set; }
        public PciData pci { get; set; }
        public AmOddsOData o { get; set; }
        public string egn { get; set; }

        [JsonProperty("p-o")]
        public List<PoData> PoData { get; set; }
        [JsonProperty("n-o")]
        public List<dynamic> NoData { get; set; }


    }
    public class AmOddsOData 
    {
        //让球
        [AmOddsOInnerDataId(1)]
        [JsonProperty("ah")]
        public AmOddsOInnerData ah { get; set; }
        //进球:大 / 小
        [AmOddsOInnerDataId(2)]
        [JsonProperty("ou")]
        public AmOddsOInnerData ou { get; set; }
        //独赢
        [AmOddsOInnerDataId(3)]
        [JsonProperty("1x2")]
        public AmOddsOInnerData x12 { get; set; }
        //让球 - 上半场
        [AmOddsOInnerDataId(4)]
        [JsonProperty("ah1st")]
        public AmOddsOInnerData ah1st { get; set; }
        //进球:大 / 小-上半场
        [AmOddsOInnerDataId(5)]
        [JsonProperty("ou1st")]
        public AmOddsOInnerData ou1st { get; set; }
        //独赢-上半场
        [AmOddsOInnerDataId(6)]
        [JsonProperty("1x21st")]
        public AmOddsOInnerData x121st { get; set; }
        //进球:单 / 双
        [AmOddsOInnerDataId(7)]
        [JsonProperty("oe")]
        public AmOddsOInnerData oe { get; set; }
        //进球:单 / 双 - 上半场
        [AmOddsOInnerDataId(8)]
        [JsonProperty("oe1st")]
        public AmOddsOInnerData oe1st { get; set; }
        //总进球数
        [AmOddsOInnerDataId(9)]
        [JsonProperty("tg")]
        public AmOddsOInnerData tg { get; set; }
        //总进球数-上半场
        [AmOddsOInnerDataId(10)]
        [JsonProperty("tg1st")]
        public AmOddsOInnerData tg1st { get; set; }
        //波胆
        [AmOddsOInnerDataId(11)]
        [JsonProperty("cs")]
        public AmOddsOInnerData cs { get; set; }
        //波胆 - 上半场
        [AmOddsOInnerDataId(12)]
        [JsonProperty("cs1st")]
        public AmOddsOInnerData cs1st { get; set; }
        //半场 / 全场
        [AmOddsOInnerDataId(13)]
        [JsonProperty("hf")]
        public AmOddsOInnerData hf { get; set; }
        //最先进球
        [AmOddsOInnerDataId(14)]
        [JsonProperty("tts1st")]
        public AmOddsOInnerData tts1st { get; set; }
        //最后进球
        [AmOddsOInnerDataId(15)]
        [JsonProperty("ttslast")]
        public AmOddsOInnerData ttslast { get; set; }
        //最先进球球员
        [AmOddsOInnerDataId(16)]
        [JsonProperty("sco1st")]
        public AmOddsOInnerData sco1st { get; set; }
        //最后进球球员
        [AmOddsOInnerDataId(17)]
        [JsonProperty("scolast")]
        public AmOddsOInnerData scolast { get; set; }
        //进球球员其他
        [AmOddsOInnerDataId(18)]
        [JsonProperty("scoant")]
        public AmOddsOInnerData scoant { get; set; }

    }
    [AttributeUsage(AttributeTargets.All,AllowMultiple = false, Inherited = true)]
    public class AmOddsOInnerDataId : Attribute
    {
        public int Id { get; set; }

        public AmOddsOInnerDataId(int id)
        {
            this.Id = id;
        }
    }
    public class AmOddsOInnerData
    {
        public int s { get; set; }
        public List<string> v { get; set; }
        public string n { get; set; }
        public string f { get; set; }
    }
    public class FbData
    {
        public string tx { get; set; }
        public string ex { get; set; }
    }
}