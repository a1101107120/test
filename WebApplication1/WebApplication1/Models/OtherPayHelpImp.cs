﻿namespace WebApplication1.Models
{
    public class OtherPayHelpImp
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Namespace { get; set; }
        public string ImpClass { get; set; }
    }
}