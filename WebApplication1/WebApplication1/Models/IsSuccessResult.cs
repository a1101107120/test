﻿namespace WebApplication1.Models
{
    public class IsSuccessResult
    {
        public IsSuccessResult()
        {
            IsSuccess = true;
        }
        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="errorMsg">错误讯息</param>
        public IsSuccessResult(string errorMsg)
        {
            IsSuccess = false;
            ErrorMessage = errorMsg;
        }

        public bool IsSuccess { get; set; }
        /// <summary>
        /// 错误讯息
        /// </summary>
        public string ErrorMessage { get; set; }
    }
    public class IsSuccessResult<T> : IsSuccessResult
    {
        public IsSuccessResult() : base()
        {

        }

        public IsSuccessResult(string errorMessage) : base(errorMessage)
        {

        }
        /// <summary>
        /// 要回传的物件
        /// </summary>
        public T ReturnObject { get; set; }

    }
}