﻿using System.Collections.Generic;

namespace WebApplication1.Models
{
    public class OtherPayInfoDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public byte PayType { get; set; }
        public List<OtherPayImp> OtherPayImpList { get; set; }
    }
}