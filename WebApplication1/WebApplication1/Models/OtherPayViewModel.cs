﻿using System.Collections.Generic;

namespace WebApplication1.Models
{
    public class OtherPayViewModel
    {
        public List<OtherPayHelpImp> OtherPayHelpImpList { get; set; }
        public List<OtherPayInfoDto> OtherPayInfoList { get; set; }
    }
}