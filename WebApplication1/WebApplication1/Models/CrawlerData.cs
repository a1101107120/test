﻿using System.Collections.Generic;
using Newtonsoft.Json;
using WebApplication1.Controllers;

namespace WebApplication1.Models
{
    public class CrawlerData
    {
        [JsonProperty("i-ot")]
        public OtData crawIot { get; set; }
        [JsonProperty("n-ot")]
        public OtData crawNot { get; set; }
        [JsonProperty("tp")]
        public int tp { get; set; }
        [JsonProperty("tf")]
        public TfData tf { get; set; }
    }
    public class OtData
    {
        public SData s { get; set; }
        public List<EgsInnerData> egs { get; set; }
        public string tn { get; set; }
        public int v { get; set; }
    }
    public class TfData
    {
        public int ot { get; set; }
        public int ov { get; set; }
        public int sb { get; set; }
    }
    public class SData
    {
        public int k { get; set; }
        public string n { get; set; }
    }
    public class EgsInnerData
    {
        public EgsInnerC c { get; set; }
        public List<OddsEsData> es { get; set; }
    }
    public class EgsInnerC
    {
        public int k { get; set; }
        public string n { get; set; }
    }
    public class OddsEsData
    {
        public bool dbg { get; set; }
        public int egid { get; set; }
        public string g { get; set; }
        public List<string> i { get; set; }
        public bool ibs { get; set; }
        public bool ibsc { get; set; }
        public int k { get; set; }
        public PciData pci { get; set; }
        public OData o { get; set; }
        public string egn { get; set; }

        [JsonProperty("p-o")]
        public List<PoData> PoData { get; set; }
        [JsonProperty("n-o")]
        public List<dynamic> NoData { get; set; }

    }
    public class PoData
    {
        public string n { get; set; }
        public string st { get; set; }
        public string f { get; set; }
        public int mgk { get; set; }
        public int s { get; set; }
        public int k { get; set; }
        public List<List<string>> o { get; set; }
    }
    public class PciData
    {
        public int pid { get; set; }
        public int ctid { get; set; }
        public string ctn { get; set; }
    }
    public class OData
    {
        [JsonProperty("ah")]
        public List<string> ah { get; set; }
        [JsonProperty("ou")]
        public List<string> ou { get; set; }
        [JsonProperty("1x2")]
        public List<string> x12 { get; set; }
        [JsonProperty("ah1st")]
        public List<string> ah1st { get; set; }
        [JsonProperty("ou1st")]
        public List<string> ou1st { get; set; }
        [JsonProperty("1x21st")]
        public List<string> x121st { get; set; }


        [JsonProperty("oe")]
        public List<string> oe { get; set; }
        [JsonProperty("oe1st")]
        public List<string> oe1st { get; set; }
        [JsonProperty("tg")]
        public List<string> tg { get; set; }
        [JsonProperty("tg1st")]
        public List<string> tg1st { get; set; }
        [JsonProperty("cs")]
        public List<string> cs { get; set; }
        [JsonProperty("cs1st")]
        public List<string> cs1st { get; set; }
        [JsonProperty("hf")]
        public List<string> hf { get; set; }
        [JsonProperty("tts1st")]
        public List<string> tts1st { get; set; }
        [JsonProperty("ttslast")]
        public List<string> ttslast { get; set; }

    }
}