﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApplication1.Extension;

namespace WebApplication1.Models
{
    public class CreateDisbursementOrderDto
    {

        public void GivenNullParameter()
        {
            if (string.IsNullOrWhiteSpace(OrderNo))
            {
                OrderNo = BaseExtensions.GetRandomOrderNo();
            }
            if (string.IsNullOrWhiteSpace(AccountNo))
            {
                AccountNo = "123456789101111";
            }
            if (string.IsNullOrWhiteSpace(LoginName))
            {
                LoginName = "Admin";
            }
            if (string.IsNullOrWhiteSpace(BankTypeCode))
            {
                BankTypeCode = "ICBC";
            }
            if (string.IsNullOrWhiteSpace(AccountName))
            {
                AccountName = "Admin-TEST";
            }
            if (string.IsNullOrWhiteSpace(AccountOpenAddress))
            {
                AccountOpenAddress = "元輝";
            }
            if (string.IsNullOrWhiteSpace(Province))
            {
                Province = "台中";
            }
        }

        [Required(ErrorMessage = "MerchantId必填")]
        public string MerchantId { get; set; }
        [Required(ErrorMessage = "MerchantPushDataId必填")]
        public string MerchantPushDataId { get; set; }

        public string OrderNo { get; set; }

        public string LoginName { get; set; } 
        public string BankTypeCode { get; set; } 
        public string AccountName { get; set; } 

        [RegularExpression(@"^\d{15}|\d{20}[0-9]", ErrorMessage = "只能數字(15/20位)")]
        public string AccountNo { get; set; }
        public string AccountOpenAddress { get; set; } 
        public string Province { get; set; }
        [Required(ErrorMessage = "手續費必填")]
        public decimal FeeAmount { get; set; }
        [Required(ErrorMessage = "金額必填")]
        public decimal Amount { get; set; }
        [Required(ErrorMessage = "密鑰必填")]
        public string Sign { get; set; }

        [Required(ErrorMessage = "本地IP必填")]
        public string LocalIp { get; set; }
    }
}