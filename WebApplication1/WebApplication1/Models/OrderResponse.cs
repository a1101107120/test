﻿namespace WebApplication1.Models
{
    public class OrderResponse
    {
        public string refreshTime { get; set; }
        public bool isSuccess { get; set; }
        public int errorCode { get; set; }
        public string message { get; set; }
        public string location { get; set; }
    }
}