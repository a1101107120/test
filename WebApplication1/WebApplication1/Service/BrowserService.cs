﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using NLog;
using PuppeteerSharp;

namespace WebApplication1.Service
{
    public class BrowserService
    {
        private IConfiguration _configuration;
        private Logger _logger;

        public BrowserService(IConfiguration configuration)
        {
            _configuration = configuration;
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        public async Task InitBrowser()
        {
            var browserFetcher = new BrowserFetcher(new BrowserFetcherOptions()
            {
                Path = Path.Combine(new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName, "Browser")
            });
            await browserFetcher.DownloadAsync(BrowserFetcher.DefaultRevision);
            using (var browser = await Puppeteer.LaunchAsync(new LaunchOptions()
            {
                Headless = true, //偵測時可設定false觀察網頁顯示結果(註：非Headless時不能匯出PDF)
                ExecutablePath = browserFetcher.RevisionInfo(BrowserFetcher.DefaultRevision).ExecutablePath
            }))
            {
                using (var page = await browser.NewPageAsync())
                {

                    await page.GoToAsync("http://demo.hzpt188.com/");
                    await page.TypeAsync("#TxtUserName", "zelda03");
                    await page.TypeAsync("#TxtPassword", "mcb123");
                    await page.ClickAsync("#loginBtn");

                    await page.WaitForNavigationAsync(new NavigationOptions()
                    {
                        WaitUntil = new WaitUntilNavigation[]
                        {
                            WaitUntilNavigation.Networkidle0
                        }
                    });

                    var vaptchaFail = await page.QuerySelectorAsync("#content");
                    await page.ScreenshotAsync($"D:\\Test\\Snapshot.png");
                    //var waitForRequestAsync = await page.WaitForRequestAsync("https://xj-mbs-yabo.q8825h.com/zh-cn/Service/MyBetService?GetMyBetCount");
                    Thread.Sleep(1000 * 3);
                    await page.ScreenshotAsync($"D:\\Test\\Snapshot2.png");
                }
            }
        }

        public async Task GetStockData()
        {
            var isRun = bool.Parse(_configuration["StockRunner"]);
            var time = int.Parse(_configuration["StockRunTime"]);
            var selector = _configuration["Selector"];
            if (isRun == false)
            {
                return;
            }
            if (DateTime.Now.Hour >= 14)
            {
                try
                {
                    var browserFetcher = new BrowserFetcher(new BrowserFetcherOptions()
                    {
                        Path = Path.Combine(new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName, "Browser")
                    });
                    await browserFetcher.DownloadAsync(BrowserFetcher.DefaultRevision);
                    using (var browser = await Puppeteer.LaunchAsync(new LaunchOptions()
                    {
                        Headless = true, //偵測時可設定false觀察網頁顯示結果(註：非Headless時不能匯出PDF)
                        ExecutablePath = browserFetcher.RevisionInfo(BrowserFetcher.DefaultRevision).ExecutablePath
                    }))
                    {
                        using (var page = await browser.NewPageAsync())
                        {
                            var url = _configuration["StockWebSiteUrl"];
                            var stockNumbers = _configuration["StockNumbers"];
                            var now = DateTime.Now.Date.ToString("yyyyMMdd");
                            foreach (var stock in stockNumbers.Split(","))
                            {
                                await page.GoToAsync(string.Format(url, stock));
                                var element = await page.QuerySelectorAsync(selector);
                                var name = await element.EvaluateFunctionAsync<string>("e => e.innerText");
                                name = name.Replace("  ", "");
                                name = name.Replace(" ", "");
                                await page.SetViewportAsync(new ViewPortOptions
                                {
                                    Width = 1920,
                                    Height = 1200
                                });
                                var path = $"{System.Environment.CurrentDirectory}\\Stock\\{name}";
                                path = path.Trim();
                                var exists = Directory.Exists(path);
                                if (exists)
                                {
                                    await page.ScreenshotAsync($"{path}\\{name}{now}.png");
                                   
                                }
                                {
                                    Directory.CreateDirectory(path);
                                    Thread.Sleep(2000);
                                    await page.ScreenshotAsync($"{path}\\{name}{now}.png");
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"截圖資訊發生錯誤:{ex}");
                }
            }
            else
            {
                _logger.Error($"截圖時間未到:{DateTime.Now}");
            }


            var nextTimer = new System.Timers.Timer();
            nextTimer.Interval = time * 60 * 1000;
            nextTimer.Elapsed += async (sender, e) =>
            {

                GetStockData();
            };
            nextTimer.AutoReset = false;
            nextTimer.Start();
           
        }
    }
}