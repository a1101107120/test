using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace WebApplication1.Service
{
    public class XJService
    {
        public static string Cookie = string.Empty;
        public static string SessionId = string.Empty;
        public static string SessionExtend = string.Empty;
        public static bool IsStart = false;
        private static readonly object CookieLock = new object();
        private readonly IHttpClientHelper _clientHelper;
        private readonly Logger _logger;
        public XJService(IHttpClientHelper clientHelper, Logger logger)
        {
            _clientHelper = clientHelper;
            _logger = logger;
        }
        private void GetCookie()
        {
            if (IsStart)
            {
                return;
            }
            Task.Run(() =>
            {
                try
                {
                    do
                    {
                        Cookie = LoginPage().Auth;
                        IsStart = true;
                        Thread.Sleep(1000 * 60);
                        using (StreamWriter sw = System.IO.File.AppendText("D:\\AddData.txt")) //小寫TXT     
                        {
                            sw.WriteLine($"{DateTime.Now} 更新COOKIE:{Cookie}");
                        }
                    } while (true);
                }
                catch (Exception e)
                {
                    _logger.Fatal("更新COOKIE失敗，稍後在嘗試");
                    Thread.Sleep(1000 * 60);
                    Cookie = LoginPage().Auth;
                }
               

            });
        }
        public (bool IsSuccess, string Auth) LoginPage()
        {
            lock (CookieLock)
            {
                try
                {
                    var httpClient = _clientHelper.GetHttpClient();
                    Console.OutputEncoding = Encoding.UTF8;
                    // 創建請求
                    httpClient.Timeout = TimeSpan.FromSeconds(10);
                    string url = "http://demo.hzpt188.com/";

                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Host", "demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Origin", "http://demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Pragma", "no-cache");

                    var response = httpClient.GetAsync(url).Result;
                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }

                    ////這句話是關鍵點
                    var cookies = response.Headers.GetValues("Set-Cookie");
                    //// 解析請求結果

                    string XSRF = cookies.ElementAt(0).Split(';')[0];
                    string Vanguard = cookies.ElementAt(1).Split(';')[0];

                    string loginCookie = XSRF + "; " + Vanguard;

                    HttpClientHandler handler = new HttpClientHandler()
                    {
                        AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                    };

                    var responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

                    string content = Reader.ReadToEnd();

                    var doc = new HtmlDocument();
                    doc.LoadHtml(content);

                    string tokenID = doc.DocumentNode.SelectNodes("//form/input").First().Attributes["value"].Value;
                    string tokenValue = doc.DocumentNode.SelectNodes("//form/input")[1].Attributes["value"].Value;

                    #region

                    httpClient.DefaultRequestHeaders.Clear();

                    url = "http://demo.hzpt188.com/login_action";
                    string postData =
                        $"xj_verify={tokenID}&_token={tokenValue}&fr_username=zelda03&fr_password=mcb123&SubmitButton=%E7%99%BB%E5%BD%95";

                    httpClient.DefaultRequestHeaders.Add("Accept",
                        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Origin", "http://demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Pragma", "no-cache");


                    var stringContent = new StringContent(postData);
                    stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                    stringContent.Headers.Add("Cookie", $"PHPSESSID=jo5bepmauqk35njplim8gg81l2; {loginCookie}");
                    stringContent.Headers.Add("Upgrade-Insecure-Requests", "1");

                    response = httpClient.PostAsync(url, stringContent).Result;
                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }
                    var cookies2 = response.Headers.GetValues("Set-Cookie");
                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    Reader = new StreamReader(responseStream, Encoding.Default);

                    content = Reader.ReadToEnd();

                    Console.WriteLine(content);

                    #endregion

                    #region 中繼登入

                    doc.LoadHtml(content);
                    httpClient.DefaultRequestHeaders.Clear();
                    string loginUrl = "http://demo.hzpt188.com" +
                                      doc.DocumentNode.SelectNodes("//form").First().Attributes["action"].Value;
                    string loginData = "data=" + HttpUtility.UrlEncode(doc.DocumentNode.SelectNodes("//form/input").First()
                        .Attributes["value"].Value);

                    cookies = response.Headers.GetValues("Set-Cookie");
                    XSRF = cookies.ElementAt(0).Split(';')[0];
                    Vanguard = cookies.ElementAt(1).Split(';')[0];

                    loginCookie = XSRF + "; " + Vanguard;

                    httpClient.DefaultRequestHeaders.Add("Accept",
                        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Origin", "http://demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/playGame/XJ/32116");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Pragma", "no-cache");

                    stringContent = new StringContent(loginData);
                    stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    stringContent.Headers.Add("Cookie", $"PHPSESSID=m5tlus0vn0q8mkacugal7tbk27; {loginCookie}");
                    stringContent.Headers.Add("Upgrade-Insecure-Requests", "1");

                    response = httpClient.PostAsync(loginUrl, stringContent).Result;
                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }
                    //var cookies3 = response.Headers.GetValues("Set-Cookie");
                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    Reader = new StreamReader(responseStream, Encoding.Default);

                    content = Reader.ReadToEnd();

                    #endregion

                    #region 中繼頁2

                    httpClient.DefaultRequestHeaders.Clear();

                    doc.LoadHtml(content);
                    string relayUrl = doc.DocumentNode.SelectNodes("//main/div/div/script")[0].InnerHtml
                        .Replace("window.location.replace(\"", "").Replace("\");", "");

                    httpClient.DefaultRequestHeaders.Add("Accept", "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "www.txg188.com");
                    httpClient.DefaultRequestHeaders.Add("Purpose", "prefetch");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");

                    response = httpClient.GetAsync(HttpUtility.UrlDecode(relayUrl)).Result;

                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }
                    var cookies4 = response.Headers.GetValues("Set-Cookie");
                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    Reader = new StreamReader(responseStream, Encoding.Default);

                    content = Reader.ReadToEnd();
                    cookies = response.Headers.GetValues("Set-Cookie");
                    string cfduid = cookies.ElementAt(0).Split(';')[0];
                    string lms_session = cookies.ElementAt(1).Split(';')[0];

                    #endregion

                    #region Launch

                    httpClient.DefaultRequestHeaders.Clear();

                    doc.LoadHtml(content);
                    string launchUrl = doc.DocumentNode.SelectNodes("//body/script")[0].InnerHtml
                        .Replace("window.location=\"", "").Replace("\";", "");

                    httpClient.DefaultRequestHeaders.Add("Accept",
                        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "xj-sb-asia.prdasbbwla2.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://www.txg188.com/");
                    httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "document");
                    httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                    httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Site", "cross-site");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    httpClient.DefaultRequestHeaders.Add("Cookie", cfduid + "; " + lms_session);

                    response = httpClient.GetAsync(HttpUtility.UrlDecode(launchUrl)).Result;

                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }

                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    cookies = response.Headers.GetValues("Set-Cookie");
                    #endregion
                    content = Unzip(responseStream);
                    doc.LoadHtml(content);
                    string headData = doc.DocumentNode.SelectNodes("//head/script")[0].InnerHtml.Replace("window.gv=", "").Split(";")[0];
                    var headDataObject = JsonConvert.DeserializeObject<JObject>(headData);

                    string auth = cookies.ElementAt(0).Split(';')[0];
                    var token = headDataObject["token"]?.Value<string>() ?? "";
                    Cookie = auth;
                    return (true, auth);

                }
                catch (Exception ex)
                {
                    _logger.Error("登入有誤", ex);
                    return (false, ex.Message);
                }
            }

        }
        private async Task<string> GetOddsAsync(string auth, int pageType)
        {
            if (string.IsNullOrEmpty(auth) == true)
            {
                return "暫無資料";
            }

            // 創建請求
            string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getodds";
            string postData = "{\"ifl\":true,\"ipf\":false,\"iip\":false,\"pn\":0,\"tz\":480,\"pt\":" + pageType + ",\"pid\":0,\"sid\":[1],\"ubt\":\"am\",\"dc\":null,\"dv\":-1,\"ot\":0}";
            var stringContent = new StringContent(postData);
            stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            stringContent.Headers.Add("Cookie", auth);

            var response = await _clientHelper.PostApi(url, stringContent, GetDefaultHeader());
            // 請求失敗
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            var cookies = response.Headers.GetValues("Set-Cookie");
            if (cookies != null)
            {
                if (cookies.Count() == 1)
                {
                    Cookie = cookies.ToList()[0].Split(";")[0];
                }
                else
                {
                    SessionId = cookies.ToList()[0].Split(";")[0];
                    Cookie = cookies.ToList()[1].Split(";")[0];
                    SessionExtend = cookies.ToList()[2].Split(";")[0];
                }

            }

            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var responseStream = response.Content.ReadAsStreamAsync().Result;
            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

            string content = Reader.ReadToEnd();

            return content;
        }
        private async Task<string> GetAmOddsAsync(string auth, int id, bool isOnline)
        {
            try
            {
                // 創建請求
                string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getamodds";
                string postData = "{\"eid\":" + id + ", \"ifl\":true, \"iip\":" + isOnline.ToString().ToLower() + ", \"isp\":false, \"ot\":2, \"ubt\":\"am\", \"tz\":480, \"v\":0}";

                var stringContent = new StringContent(postData);
                stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                stringContent.Headers.Add("Cookie", auth);

                var response = await _clientHelper.PostApi(url, stringContent, GetDefaultHeader()).ConfigureAwait(false);
                // 請求失敗
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                    return string.Empty;
                }

                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                var responseStream = response.Content.ReadAsStreamAsync().Result;
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                string content = Reader.ReadToEnd();
                return content;

            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
                throw;
            }
        }
        private string Unzip(Stream gZipStream)
        {
            gZipStream = new GZipStream(gZipStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(gZipStream, Encoding.Default);

            string content = Reader.ReadToEnd();

            return content;
        }
        public async Task Start()
        {
            try
            {
                if (IsStart == false)
                {
                    GetCookie();
                }

                while (string.IsNullOrEmpty(Cookie))
                {
                    Thread.Sleep(1000);
                }

                Task.Run(() =>
                {
                    new XJFootBallService(_clientHelper, _logger).StartRun();
                });


            }
            catch (Exception e)
            {
                _logger.Error($"Login()發生錯誤:{e}");
            }
        }
        private Dictionary<string, string> GetDefaultHeader()
        {
            var headerDic = new Dictionary<string, string>();
            headerDic.Add("Accept-Encoding", "gzip, deflate");
            headerDic.Add("Accept", "*/*");
            headerDic.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
            headerDic.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
            return headerDic;
        }

    }
    public class XJFootBallService
    {
        private readonly IHttpClientHelper _clientHelper;
        private readonly Logger _logger;
        private readonly string _getOddsUrl = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getodds";
        private readonly string _getAmOddsUrl = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getamodds";
        public XJFootBallService(IHttpClientHelper clientHelper, Logger logger)
        {
            _clientHelper = clientHelper;
            _logger = logger;
        }

        public void StartRun()
        {
            var pageTypeDic = new Dictionary<int, int>()
            {
                {1,20},
                {2,60},
                {3,60}
            };
            foreach (var pageType in pageTypeDic)
            {
                Task.Run(() =>
                {
                    GetOdds(XJService.Cookie, pageType);
                });
            }

        }

        public void GetOdds(string cookie, KeyValuePair<int, int> pageType)
        {
           
        }

    }
}