using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication1.Service
{
    public interface IHttpClientHelper
    {
        Task<HttpResponseMessage> PostApi(string url, StringContent stringContent, Dictionary<string, string> headerDic);
        HttpClient GetHttpClient();
    }
    public class HttpClientHelper : IHttpClientHelper
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly HttpClient _client;

        public HttpClientHelper(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            _client = _clientFactory.CreateClient();
        }

        public HttpClient GetHttpClient()
        {
            return _clientFactory.CreateClient(); ;
        }

        public async Task<HttpResponseMessage> PostApi(string url, StringContent stringContent,
            Dictionary<string, string> headerDic )
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
           
            request.Content = stringContent;
            if (headerDic!=null)
            {
                foreach (var data in headerDic)
                {
                    request.Headers.Add(data.Key,data.Value);
                }
            }
            
            var response = await _client.SendAsync(request);
            return response;
        }

        private void SetDefaultHeader(HttpRequestMessage request)
        {
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
            _client.DefaultRequestHeaders.Add("Accept", "*/*");
            _client.DefaultRequestHeaders.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
            _client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
        }

    }
}