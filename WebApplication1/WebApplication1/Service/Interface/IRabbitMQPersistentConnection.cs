﻿using System;
using RabbitMQ.Client;

namespace WebApplication1.Service.Interface
{
    public interface IRabbitMQPersistentConnection
    {
        Boolean IsConnected { get; }

        Boolean TryConnect();

        IModel CreateModel();
    }
    public interface IEventBus
    {
        void Publish(object obj);
    }
}