﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using Library.ZdSportModel;
using Microsoft.AspNetCore.Mvc;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using MQTTnet.Client.Subscribing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using WebApplication1.Logic;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    public class BugController : Controller
    {
        public static string Cookie = string.Empty;
        public static string Token = string.Empty;
        public static bool IsStart = false;
        private readonly Logger _logger;
        public static string SessionId = string.Empty;
        public static string SessionExtend = string.Empty;
        private readonly IHttpClientHelper _clientHelper;
        private readonly IZdSportLogic _zdSportLogic;
        private static object CookieLock = new object();
        public BugController(IHttpClientHelper clientHelper, IZdSportLogic zdSportLogic)
        {
            _clientHelper = clientHelper;
            _zdSportLogic = zdSportLogic;
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        private void GetCookie()
        {
            if (IsStart)
            {
                return;
            }
            Task.Run(() =>
            {
                do
                {
                    Cookie = LoginPage().Auth;
                    IsStart = true;
                    Thread.Sleep(1000 * 60);
                    using (StreamWriter sw = System.IO.File.AppendText("D:\\AddData.txt")) //小寫TXT     
                    {
                        sw.WriteLine($"{DateTime.Now} 更新COOKIE:{Cookie}");
                    }
                } while (true);

            });
        }
        //模擬小金登入
        public IActionResult GetLoginInfo()
        {
            var cookies = new CookieContainer();
            string url = "http://demo.hzpt188.com/";  //驗證碼頁面
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Accept = "*/*";
            request.Method = "GET";
            request.UserAgent = "Mozilla/5.0";
            request.CookieContainer = new CookieContainer(); //暫存到新例項
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            HtmlDocument hDoc = new HtmlDocument();
            MemoryStream ms = null;

            using (var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            {
                var responseText = streamReader.ReadToEnd();
                hDoc.LoadHtml(responseText);
            }
            var xj_verifyData = hDoc.DocumentNode.SelectSingleNode("//input[@name=\"xj_verify\"]");
            var tokenData = hDoc.DocumentNode.SelectSingleNode("//input[@name=\"_token\"]");
            var token = tokenData.Attributes.Contains("value") ? tokenData.Attributes["value"].Value : string.Empty;
            var xj_verify = xj_verifyData.Attributes.Contains("value") ? xj_verifyData.Attributes["value"].Value : string.Empty;
            cookies = request.CookieContainer; //儲存cookies
            var strCookies = request.CookieContainer.GetCookieHeader(request.RequestUri); //把cookies轉換成字串

            var account = "zelda03";
            var password = "mcb123";
            var loginResponse = Login(account, password, xj_verify, token, strCookies);


            return new JsonResult(loginResponse);
        }
        private string Login(string account, string password, string xjVerify, string token, string cookies)
        {
            var xsrf = cookies.Split(";")[0];
            var vanguard = cookies.Split(";")[1];
            var url = "http://demo.hzpt188.com/login_action";
            //var cookieContainer = new CookieContainer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            //cookieContainer.Add(new Cookie(xsrf.Split("=")[0], xsrf.Split("=")[1]));
            //cookieContainer.Add(new Cookie(vanguard.Split("=")[0], vanguard.Split("=")[1]));
            //request.CookieContainer = cookieContainer;
            request.Headers["Cookie"] = cookies;
            request.UserAgent = "Mozilla/5.0";
            request.Referer = "http://demo.hzpt188.com";

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("xj_verify", xjVerify);
            postParams.Add("_token", token);
            postParams.Add("fr_username", "zelda03");
            postParams.Add("fr_password", "mcb123");
            postParams.Add("SubmitButton", "登入");


            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.WriteAsync(byteArray, 0, byteArray.Length);
            }
            request.ContentLength = byteArray.Length;

            //發出Request
            string responseStr;
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    responseStr = sr.ReadToEnd();
                }
            }



            return responseStr;
        }
        public async Task<IActionResult> Crawler()
        {

            var odds = string.Empty;
            if (IsStart)
            {
                odds = await GetOddsAsync(Cookie, 1);
            }
            else
            {
                var authookie = LoginPage().Auth;
                // XJWSService().GetAwaiter().GetResult();
                odds = await GetOddsAsync(authookie, 1);
                //PartialConnect().GetAwaiter().GetResult();
                //GetCookie();
            }


            var deserializeObject = JsonConvert.DeserializeObject<CrawlerData>(odds);
            return View(deserializeObject);
        }
        public async Task<IActionResult> AmOddView(int id, bool isOnline)
        {
            CookieContainer cookieContainer = new CookieContainer();
            var handler1 = new HttpClientHandler()
            {
                CookieContainer = cookieContainer,
                AllowAutoRedirect = true,
                UseCookies = true,
                ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true,
            };
            var client = new HttpClient(handler1);
            var amOdds = string.Empty;
            if (IsStart)
            {
                amOdds = await GetAmOddsAsync(Cookie, id, isOnline, client);
            }
            else
            {
                var authookie = LoginPage().Auth;
                amOdds = await GetAmOddsAsync(authookie, id, isOnline, client);
                //GetCookie();
            }

            var deserializeObject = JsonConvert.DeserializeObject<CrawlerAmOddsOnlineData>(amOdds);
            return PartialView("_IotPartialView", deserializeObject);

        }
        public async Task<IActionResult> Login()
        {
            try
            {
                if (IsStart == false)
                {
                    GetCookie();
                }

                while (string.IsNullOrEmpty(Cookie))
                {
                    Thread.Sleep(1000);
                }
                var pageTypeDic = new Dictionary<int, int>()
                {
                    {1,20},
                    {2,60},
                    {3,60}
                };
                foreach (var pageType in pageTypeDic)
                {
                    Task.Run(async () =>
                    {
                        await AmOddsRobot(pageType);
                    });
                }
            }
            catch (Exception e)
            {
                _logger.Error($"Login()發生錯誤:{e}");
            }

            return Json(null);
        }

        public async Task AmOddsRobot(KeyValuePair<int, int> pageType)
        {
            var odds = await GetOddsAsync(Cookie, pageType.Key);
            await GetAmOdds(odds, pageType.Key);

            System.IO.File.WriteAllText(@$"D:\LoginInfo{pageType}.txt", odds);
            var nextTimer = new System.Timers.Timer();
            nextTimer.Interval = pageType.Value * 1000;
            nextTimer.Elapsed += async (sender, e) =>
            {
                await AmOddsRobot(pageType);
            };
            nextTimer.AutoReset = false;
            nextTimer.Start();
        }
        public IActionResult GetAmOdd()
        {
            var odds = System.IO.File.ReadAllText(@"D:\LoginInfo.txt");
            GetAmOdds(odds, 1).GetAwaiter().GetResult();
            return null;
        }
        public async Task<IActionResult> PlaceBet(string slid, string eid, string ods, string scr, string iip)
        {

            var pbSlid = slid.Substring(1);
            var pbScr = scr;
            var pbEid = int.Parse(eid);
            var pbOds = Convert.ToDouble(ods);
            var pbPid = 0;
            var pbIip = iip == "true";

            // 創建請求
            string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/placebet";
            string post = "{\"ep\":[{\"pid\":" + pbPid + ",\"slid\":\"" + pbSlid + "\",\"eid\":" + pbEid + ",\"ods\":" + pbOds + ",\"scr\":\"" + pbScr + "\",\"iip\":" + iip + "}],\"am\":[10],\"ew\":[],\"is\":true}";
            var headerDic = GetDefaultHeader();
            headerDic.Add("X-Requested-With", "XMLHttpRequest");
            headerDic.Add("Connection", "keep-alive");
            headerDic.Add("Sec-Fetch-Mode", "cors");
            headerDic.Add("Sec-Fetch-Site", "same-origin");
            headerDic.Add("Sec-Fetch-Dest", "empty");
            headerDic.Add("Host", "xj-sb-asia.prdasbbwla2.com");
            headerDic.Add("Origin", "https://xj-sb-asia.prdasbbwla2.com");

            var stringContent = new StringContent(post);
            stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            stringContent.Headers.Add("Cookie", Cookie);

            var response = await _clientHelper.PostApi(url, stringContent, headerDic);
            //var response = await _httpClient.PostAsync(url, stringContent).ConfigureAwait(false);
            // 請求失敗
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }

            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var responseStream = response.Content.ReadAsStreamAsync().Result;
            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

            string content = Reader.ReadToEnd();
            var sss = stringContent.ToString();

            return null;
        }
        public (bool IsSuccess, string Auth) LoginPage()
        {
            lock (CookieLock)
            {
                try
                {

                    CookieContainer cookieContainer = new CookieContainer();

                    var handler1 = new HttpClientHandler()
                    {
                        CookieContainer = cookieContainer,
                        AllowAutoRedirect = true,
                        UseCookies = true,
                        ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
                    };

                    var httpClient = _clientHelper.GetHttpClient();
                    Console.OutputEncoding = Encoding.UTF8;
                    // 創建請求
                    httpClient.Timeout = TimeSpan.FromSeconds(10);
                    string url = "http://demo.hzpt188.com/";

                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Host", "demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Origin", "http://demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Pragma", "no-cache");

                    var response = httpClient.GetAsync(url).Result;
                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }

                    ////這句話是關鍵點
                    var cookies = response.Headers.GetValues("Set-Cookie");
                    //// 解析請求結果

                    string XSRF = cookies.ElementAt(0).Split(';')[0];
                    string Vanguard = cookies.ElementAt(1).Split(';')[0];

                    string loginCookie = XSRF + "; " + Vanguard;

                    HttpClientHandler handler = new HttpClientHandler()
                    {
                        AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                    };

                    var responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

                    string content = Reader.ReadToEnd();

                    var doc = new HtmlDocument();
                    doc.LoadHtml(content);

                    string tokenID = doc.DocumentNode.SelectNodes("//form/input").First().Attributes["value"].Value;
                    string tokenValue = doc.DocumentNode.SelectNodes("//form/input")[1].Attributes["value"].Value;

                    #region

                    httpClient.DefaultRequestHeaders.Clear();

                    url = "http://demo.hzpt188.com/login_action";
                    string postData =
                        $"xj_verify={tokenID}&_token={tokenValue}&fr_username=zelda03&fr_password=mcb123&SubmitButton=%E7%99%BB%E5%BD%95";

                    httpClient.DefaultRequestHeaders.Add("Accept",
                        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Origin", "http://demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Pragma", "no-cache");


                    var stringContent = new StringContent(postData);
                    stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                    stringContent.Headers.Add("Cookie", $"PHPSESSID=jo5bepmauqk35njplim8gg81l2; {loginCookie}");
                    stringContent.Headers.Add("Upgrade-Insecure-Requests", "1");

                    response = httpClient.PostAsync(url, stringContent).Result;
                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }
                    var cookies2 = response.Headers.GetValues("Set-Cookie");
                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    Reader = new StreamReader(responseStream, Encoding.Default);

                    content = Reader.ReadToEnd();

                    Console.WriteLine(content);

                    #endregion

                    #region 中繼登入

                    doc.LoadHtml(content);
                    httpClient.DefaultRequestHeaders.Clear();
                    string loginUrl = "http://demo.hzpt188.com" +
                                      doc.DocumentNode.SelectNodes("//form").First().Attributes["action"].Value;
                    string loginData = "data=" + HttpUtility.UrlEncode(doc.DocumentNode.SelectNodes("//form/input").First()
                        .Attributes["value"].Value);

                    cookies = response.Headers.GetValues("Set-Cookie");
                    XSRF = cookies.ElementAt(0).Split(';')[0];
                    Vanguard = cookies.ElementAt(1).Split(';')[0];

                    loginCookie = XSRF + "; " + Vanguard;

                    httpClient.DefaultRequestHeaders.Add("Accept",
                        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Origin", "http://demo.hzpt188.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/playGame/XJ/32116");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Pragma", "no-cache");

                    stringContent = new StringContent(loginData);
                    stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    stringContent.Headers.Add("Cookie", $"PHPSESSID=m5tlus0vn0q8mkacugal7tbk27; {loginCookie}");
                    stringContent.Headers.Add("Upgrade-Insecure-Requests", "1");

                    response = httpClient.PostAsync(loginUrl, stringContent).Result;
                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }
                    //var cookies3 = response.Headers.GetValues("Set-Cookie");
                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    Reader = new StreamReader(responseStream, Encoding.Default);

                    content = Reader.ReadToEnd();

                    #endregion

                    #region 中繼頁2

                    httpClient.DefaultRequestHeaders.Clear();

                    doc.LoadHtml(content);
                    string relayUrl = doc.DocumentNode.SelectNodes("//main/div/div/script")[0].InnerHtml
                        .Replace("window.location.replace(\"", "").Replace("\");", "");

                    httpClient.DefaultRequestHeaders.Add("Accept", "application/signed-exchange;v=b3;q=0.9,*/*;q=0.8");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "www.txg188.com");
                    httpClient.DefaultRequestHeaders.Add("Purpose", "prefetch");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://demo.hzpt188.com/");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");

                    response = httpClient.GetAsync(HttpUtility.UrlDecode(relayUrl)).Result;

                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }
                    var cookies4 = response.Headers.GetValues("Set-Cookie");
                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    Reader = new StreamReader(responseStream, Encoding.Default);

                    content = Reader.ReadToEnd();
                    cookies = response.Headers.GetValues("Set-Cookie");
                    string cfduid = cookies.ElementAt(0).Split(';')[0];
                    string lms_session = cookies.ElementAt(1).Split(';')[0];

                    #endregion

                    #region Launch

                    httpClient.DefaultRequestHeaders.Clear();

                    doc.LoadHtml(content);
                    string launchUrl = doc.DocumentNode.SelectNodes("//body/script")[0].InnerHtml
                        .Replace("window.location=\"", "").Replace("\";", "");

                    httpClient.DefaultRequestHeaders.Add("Accept",
                        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                    httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
                    httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
                    httpClient.DefaultRequestHeaders.Add("Host", "xj-sb-asia.prdasbbwla2.com");
                    httpClient.DefaultRequestHeaders.Add("Referer", "http://www.txg188.com/");
                    httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "document");
                    httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                    httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Site", "cross-site");
                    httpClient.DefaultRequestHeaders.Add("User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                    httpClient.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    httpClient.DefaultRequestHeaders.Add("Cookie", cfduid + "; " + lms_session);

                    response = httpClient.GetAsync(HttpUtility.UrlDecode(launchUrl)).Result;

                    // 請求失敗
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(response.StatusCode);
                    }

                    responseStream = response.Content.ReadAsStreamAsync().Result;
                    //responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                    //Reader = new StreamReader(responseStream, Encoding.Default);

                    //content = Reader.ReadToEnd();
                    //cookies = response.Headers.GetValues("Set-Cookie");
                    cookies = response.Headers.GetValues("Set-Cookie");
                    #endregion
                    content = Unzip(responseStream);
                    doc.LoadHtml(content);
                    string headData = doc.DocumentNode.SelectNodes("//head/script")[0].InnerHtml.Replace("window.gv=", "").Split(";")[0];
                    var headDataObject = JsonConvert.DeserializeObject<JObject>(headData);

                    string auth = cookies.ElementAt(0).Split(';')[0];
                    var token = headDataObject["token"]?.Value<string>() ?? "";
                    Cookie = auth;
                    Token = token;
                    return (true, auth);

                }
                catch (Exception ex)
                {
                    _logger.Error("登入有誤", ex);
                    return (false, ex.Message);
                }
            }
        }
        private async Task<string> GetYabo(HttpClient httpClient)
        {
            try
            {
                //Thread.Sleep(5);
                // 創建請求
                httpClient.DefaultRequestHeaders.Clear();
                var timestamp = ((long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds).ToString();
                string url = "https://xj-mbs-yabo.q8825h.com/zh-cn/Service/CentralService?GetData&ts=" + timestamp;
                httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                httpClient.DefaultRequestHeaders.Add("Referer", "https://xj-mbs-yabo.q8825h.com/m/zh-cn/sports/football/competition/full-time-asian-handicap-and-over-under?sc=ABHAJH&competitionids=26726&theme=YABO");
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
                httpClient.DefaultRequestHeaders.Add("Cookie", " timeZone=480; ASP.NET_SessionId=byazs2wpua4fnfasike5wash; mc=WL67Anickyabo1; opCode=YABO; lobbyUrl=#; logoutUrl=https://www.yabo414.com; sbmwl3-yabo=1628507914.20480.0000; settingProfile=OddsType=2&NoOfLinePerEvent=1&SortBy=1&AutoRefreshBetslip=False; fav3=; historyUrl=%2Fm%2Fzh-cn%2Fsports%2Ftable-tennis%2Fin-play%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAIB%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2Ffinancial-bets%2Fin-play%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAIB%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2Ftable-tennis%2Fin-play%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAIB%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2F4495402%2Fin-play%3Fsc%3DABHAIB%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2Ffootball%2Fin-play%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAJJ%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2F1%2Fin-play%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAJJ%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2F4500731%2Fin-play%3Fsc%3DABHAJH%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2F1%2Fin-play%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAJH%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2Ffootball%2Fselect-competition%2Fdefault%3Fsc%3DABHAJH%26theme%3DYABO%7C%2Fm%2Fzh-cn%2Fsports%2Ffootball%2Fcompetition%2Ffull-time-asian-handicap-and-over-under%3Fsc%3DABHAJH%26competitionids%3D26726%26theme%3DYABO");

                var formData = new MultipartFormDataContent();
                formData.Add(new StringContent("true"), "IsFirstLoad");
                formData.Add(new StringContent("0"), "VersionH");
                formData.Add(new StringContent("false"), "IsEventMenu");
                formData.Add(new StringContent("1"), "SportID");
                formData.Add(new StringContent("-1"), "CompetitionID");
                formData.Add(new StringContent("/m/zh-cn/sports/football/competition/full-time-asian-handicap-and-over-under?competitionids=26726"), "reqUrl");
                formData.Add(new StringContent("true"), "IsMobile");
                formData.Add(new StringContent("0"), "oCompetitionId");
                formData.Add(new StringContent("0"), "oEventIds");
                formData.Add(new StringContent("true"), "oIsFirstLoad");
                formData.Add(new StringContent("0"), "oPageNo");
                formData.Add(new StringContent("1"), "oSortBy");
                formData.Add(new StringContent("0"), "LiveCenterEventId");
                formData.Add(new StringContent("1"), "LiveCenterSportId");



                var response = await httpClient.PostAsync(url, formData).ConfigureAwait(false);
                // 請求失敗
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                    return string.Empty;
                }

                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                var responseStream = response.Content.ReadAsStreamAsync().Result;
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                string content = Reader.ReadToEnd();
                return content;

            }
            catch (TaskCanceledException ex)
            {
                Thread.Sleep(1000);
                throw;

            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
                throw;
            }
        }
        private async Task<string> GetAmOddsAsync(string auth, int id, bool isOnline, HttpClient httpClient)
        {
            try
            {
                // 創建請求
                httpClient.DefaultRequestHeaders.Clear();

                string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getamodds";
                string postData = "{\"eid\":" + id + ", \"ifl\":true, \"iip\":" + isOnline.ToString().ToLower() + ", \"isp\":false, \"ot\":2, \"ubt\":\"am\", \"tz\":480, \"v\":0}";
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                httpClient.DefaultRequestHeaders.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
                httpClient.DefaultRequestHeaders.Add("Cookie", auth);

                var stringContent = new StringContent(postData, Encoding.UTF8, "application/json");


                var response = await httpClient.PostAsync(url, stringContent).ConfigureAwait(false);
                // 請求失敗
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                    return string.Empty;
                }

                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                var responseStream = response.Content.ReadAsStreamAsync().Result;
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                string content = Reader.ReadToEnd();
                return content;

            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
                throw;
            }
        }
        private async Task<string> GetAmOddsAsync(string auth, int id, bool isOnline, IHttpClientHelper httpClient)
        {
            try
            {
                // 創建請求
                string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getamodds";
                string postData = "{\"eid\":" + id + ", \"ifl\":true, \"iip\":" + isOnline.ToString().ToLower() + ", \"isp\":false, \"ot\":2, \"ubt\":\"am\", \"tz\":480, \"v\":0}";
                var headerDic = new Dictionary<string, string>();
                headerDic.Add("Accept-Encoding", "gzip, deflate");
                headerDic.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
                headerDic.Add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");

                var stringContent = new StringContent(postData);
                stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                stringContent.Headers.Add("Cookie", auth);

                var response = await httpClient.PostApi(url, stringContent, headerDic).ConfigureAwait(false);
                // 請求失敗
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                    return string.Empty;
                }

                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                var responseStream = response.Content.ReadAsStreamAsync().Result;
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                string content = Reader.ReadToEnd();
                return content;

            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
                throw;
            }
        }
        private async Task<string> GetAmOddsAsync(string auth, int id, bool isOnline, HttpClient httpClient, string ip)
        {
            try
            {
                //Thread.Sleep(5);
                // 創建請求
                httpClient.DefaultRequestHeaders.Clear();

                string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getamodds";
                string postData = "{\"eid\":" + id + ", \"ifl\":true, \"iip\":" + isOnline.ToString().ToLower() + ", \"isp\":false, \"ot\":2, \"ubt\":\"am\", \"tz\":480, \"v\":0}";
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                httpClient.DefaultRequestHeaders.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
                httpClient.DefaultRequestHeaders.Add("Cookie", auth);
                httpClient.DefaultRequestHeaders.Add("x-forwarded-for", ip);
                httpClient.DefaultRequestHeaders.Add("remote_addr", ip);

                var stringContent = new StringContent(postData, Encoding.UTF8, "application/json");


                var response = await httpClient.PostAsync(url, stringContent).ConfigureAwait(false);
                // 請求失敗
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                }

                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                var responseStream = response.Content.ReadAsStreamAsync().Result;
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                string content = Reader.ReadToEnd();
                return content;

            }
            catch (TaskCanceledException ex)
            {
                Thread.Sleep(1000);
                throw;

            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
                throw;
            }
        }
        private async Task<string> GetOddsAsync(string auth, int pageType)
        {
            if (string.IsNullOrEmpty(auth) == true)
            {
                return "暫無資料";
            }

            // 創建請求
            string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/getodds";
            string postData = "{\"ifl\":true,\"ipf\":false,\"iip\":false,\"pn\":0,\"tz\":480,\"pt\":" + pageType + ",\"pid\":0,\"sid\":[1],\"ubt\":\"am\",\"dc\":null,\"dv\":-1,\"ot\":0}";
            var stringContent = new StringContent(postData);
            stringContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            stringContent.Headers.Add("Cookie", auth);

            var response = await _clientHelper.PostApi(url, stringContent, GetDefaultHeader());
            // 請求失敗
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            var cookies = response.Headers.GetValues("Set-Cookie");
            if (cookies != null)
            {
                if (cookies.Count() == 1)
                {
                    Cookie = cookies.ToList()[0].Split(";")[0];
                }
                else
                {
                    SessionId = cookies.ToList()[0].Split(";")[0];
                    Cookie = cookies.ToList()[1].Split(";")[0];
                    SessionExtend = cookies.ToList()[2].Split(";")[0];
                }

            }

            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var responseStream = response.Content.ReadAsStreamAsync().Result;
            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

            string content = Reader.ReadToEnd();

            return content;
        }
        private async Task PartialConnect()
        {
            CookieContainer cookieContainer = new CookieContainer();
            var handler1 = new HttpClientHandler()
            {
                CookieContainer = cookieContainer,
                AllowAutoRedirect = true,
                UseCookies = true,
                ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
            };

            var httpClient = new HttpClient(handler1);
            // 創建請求
            httpClient.DefaultRequestHeaders.Clear();

            string url = "https://xj-sb-asia.prdasbbwla2.com/zh-cn/serv/partialconnect";
            httpClient.DefaultRequestHeaders.Add("Accept", "*/*");
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            httpClient.DefaultRequestHeaders.Add("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7");
            httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
            httpClient.DefaultRequestHeaders.Add("Host", "xj-sb-asia.prdasbbwla2.com");
            httpClient.DefaultRequestHeaders.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
            httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Dest", "empty");
            httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "cors");
            httpClient.DefaultRequestHeaders.Add("Sec-Fetch-Site", "same-origin");
            httpClient.DefaultRequestHeaders.Add("Cookie", Cookie);

            var response = await httpClient.GetAsync(url).ConfigureAwait(false);
            // 請求失敗
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            var cookies = response.Headers.GetValues("Set-Cookie");
            if (cookies.ToList().Count == 1)
            {
                Cookie = cookies.ToList()[0].Split(";")[0];
            }
            else
            {
                SessionId = cookies.ToList()[0].Split(";")[0];
                SessionExtend = cookies.ToList()[2].Split(";")[0];
                Cookie = cookies.ToList()[1].Split(";")[0];
            }

            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var responseStream = response.Content.ReadAsStreamAsync().Result;
            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

            string content = Reader.ReadToEnd();
        }
        private async Task GetAmOdds(string odds, int pageType)
        {
            if (string.IsNullOrWhiteSpace(odds))
            {
                return;
            }
            try
            {
                var deserializeObject = JsonConvert.DeserializeObject<CrawlerData>(odds);
                var crawIot = new List<string>();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                var count = 15;
                if (deserializeObject.crawIot != null && deserializeObject.crawIot.egs != null)
                {
                    var list = deserializeObject.crawIot.egs.Where(r => r.es != null)
                        .SelectMany(r => r.es).Select(r => r.k).ToList();
                    var thread = (int)Math.Ceiling((decimal)list.Count / count);
                    var tasks = new List<Task<string>>();

                    for (int i = 0; i < thread; i++)
                    {

                        foreach (var item in list.Skip(i * count).Take(count))
                        {
                            tasks.Add(GetAmOddsAsync(Cookie, item, true, _clientHelper));
                        }
                    }

                    crawIot.AddRange((await Task.WhenAll(tasks)).ToList());
                }

                var crawNot = new List<string>();
                if (deserializeObject.crawNot != null && deserializeObject.crawNot.egs != null)
                {
                    var list = deserializeObject.crawNot.egs.Where(r => r.es != null)
                        .SelectMany(r => r.es).Select(r => r.k).ToList();
                    var thread = (int)Math.Ceiling((decimal)list.Count / count);
                    var tasks = new List<Task<string>>();

                    for (int i = 0; i < thread; i++)
                    {
                        foreach (var item in list.Skip(i * count).Take(count))
                        {
                            tasks.Add(GetAmOddsAsync(Cookie, item, false, _clientHelper));
                        }
                    }

                    crawNot.AddRange((await Task.WhenAll(tasks)).ToList());
                }

                sw.Stop();
                var elapsed = sw.Elapsed;

                Stopwatch sw2 = new Stopwatch();
                sw2.Start();
                SetDataToDb(crawIot, true, pageType);
                SetDataToDb(crawNot, false, pageType);
                sw2.Stop();
                var sw2Elapsed = sw2.Elapsed;
            }
            catch (Exception e)
            {
                _logger.Debug(e);
                throw;
            }
        }
        private void SetDataToDb(List<string> dataList, bool isProcessing, int pageType)
        {
            if (dataList.Count == 0)
            {
                _logger.Debug($"{DateTime.Now} 取得GetAmodds為空");
                return;
            }
            //地區盤口
            var areaTypeCode = 1;
            //賠率分類
            var pageTypeCode = pageType;
            var leagueTypeList = new List<LeagueType>();
            var oddInfoList = new List<OddInfo>();
            var moreOddInfoList = new List<MoreOddInfo>();
            var gameInfoList = new List<GameInfo>();

            foreach (var data in dataList)
            {
                var deserializeObject = JsonConvert.DeserializeObject<CrawlerAmOddsOnlineData>(data);

                if (leagueTypeList.FirstOrDefault(r => r.Code == deserializeObject.eg.c.k) == null)
                {
                    leagueTypeList.Add(new LeagueType()
                    {
                        Code = deserializeObject.eg.c.k,
                        Name = deserializeObject.eg.c.n
                    });
                }

                foreach (var amOddsEsData in deserializeObject.eg.es)
                {
                    if (gameInfoList.FirstOrDefault(r => r.Id == amOddsEsData.k.ToString()) != null)
                    {
                        continue;
                    }
                    var gameInfo = new GameInfo()
                    {
                        Id = amOddsEsData.k.ToString(),
                        LeagueTypeCode = deserializeObject.eg.c.k,
                        ScoreTypeCode = amOddsEsData.pci.ctid,
                        HomeTeam = amOddsEsData.i[0],
                        AwayTeam = amOddsEsData.i[1],
                        Mores = amOddsEsData.i[2],
                        Date = amOddsEsData.i[4],
                        Time = amOddsEsData.i[5],
                        HomeTeamRed = amOddsEsData.i[8],
                        AwayTeamRed = amOddsEsData.i[9],
                        HomeTeamScore = amOddsEsData.i[10],
                        AwayTeamScore = amOddsEsData.i[11],
                        PartCh = amOddsEsData.i[12],
                        PartEn = amOddsEsData.i[13],
                        HomeTeamBingo = amOddsEsData.i[17],
                        AwayTeamBingo = amOddsEsData.i[18],
                        IsProcessing = isProcessing,
                        AreaTypeCode = areaTypeCode,
                        PageTypeCode = pageTypeCode,
                        ScoreCode = amOddsEsData.pci.ctid,
                        ScoreTypeName = amOddsEsData.pci.ctn ?? "一般",

                    };
                    gameInfoList.Add(gameInfo);

                    var propertyInfos = amOddsEsData.o.GetType().GetProperties();
                    //組賠率
                    foreach (var propertyInfo in propertyInfos)
                    {
                        var amOddsOInnerData = (AmOddsOInnerData)propertyInfo.GetValue(amOddsEsData.o);

                        if (amOddsOInnerData == null)
                        {
                            continue;
                        }

                        var jsonPropertyAttribute = propertyInfo.CustomAttributes.FirstOrDefault(r => r.AttributeType.Name == nameof(JsonPropertyAttribute));
                        var property = typeof(AmOddsOData).GetProperty(propertyInfo.Name);
                        var amOddsOInnerDataId = (AmOddsOInnerDataId)Attribute.GetCustomAttribute(property, typeof(AmOddsOInnerDataId));
                        oddInfoList.Add(new OddInfo()
                        {
                            AreaTypeCode = areaTypeCode,
                            PageTypeCode = pageTypeCode,
                            GemeInfoId = amOddsEsData.k.ToString(),
                            Odds = JsonConvert.SerializeObject(amOddsOInnerData.v),
                            OddTypeCode = amOddsOInnerDataId.Id,
                            OddsName = amOddsOInnerData.n,
                            OddsPropertyName = jsonPropertyAttribute.ConstructorArguments.FirstOrDefault().Value.ToString()
                        });

                    }
                    //更多賠率
                    if (amOddsEsData.PoData != null)
                    {
                        moreOddInfoList.Add(new MoreOddInfo()
                        {
                            AreaTypeCode = areaTypeCode,
                            PageTypeCode = pageTypeCode,
                            GemeInfoId = amOddsEsData.k.ToString(),
                            MoreOdds = JsonConvert.SerializeObject(amOddsEsData.PoData),
                            OddTypeCode = 0,
                        });
                    }

                }

            }

            _zdSportLogic.AddDataToTable(leagueTypeList, oddInfoList, moreOddInfoList, gameInfoList);
            using (StreamWriter sw = System.IO.File.AppendText($"D:\\AddData-{pageType}.txt")) //小寫TXT     
            {
                sw.WriteLine($"{DateTime.Now} 寫入資料。 PageType:{pageType}, IsProcess:【 {isProcessing} 】, League:{leagueTypeList.Count}, GameInfo:{gameInfoList.Count}, MoreOdd:{moreOddInfoList.Count}, Odds:{oddInfoList.Count}");
            }

        }
        private async Task XJWSService()
        {
            var factory = new MqttFactory();
            var mqttClient = factory.CreateMqttClient();

            string clientId = Token;
            // Use WebSocket connection.
            var options = new MqttClientOptionsBuilder()
                .WithClientId($"{clientId}=mobile")
                .WithWebSocketServer("xj-mybet-cnt-mq-wss.prdasbbwla1.com/ws")
                .WithCredentials(clientId, clientId)
                .Build();

            var result = await mqttClient.ConnectAsync(options);

            mqttClient.UseConnectedHandler(e =>
            {
                Debug.WriteLine("### Connected ###");
            });

            mqttClient.UseApplicationMessageReceivedHandler(e =>
            {
                string message = Unzip(e.ApplicationMessage.Payload);

                Debug.WriteLine($"### RECEIVED MESSAGE ### {e.ApplicationMessage.Topic}:" + message);

                if (e.ApplicationMessage.Topic.Contains("partial"))
                {
                    //PartialConnect();
                }
            });

            MqttClientSubscribeOptions mqttClientSubscribeOptions = new MqttClientSubscribeOptions();
            await mqttClient.SubscribeAsync($"{clientId}.partial");
            await mqttClient.SubscribeAsync($"{clientId}.balance");

        }
        public async Task<IActionResult> TestHeader()
        {
            try
            {
                using (StreamWriter sw = System.IO.File.AppendText("D:\\TestFile.txt")) //小寫TXT     
                {
                    var loginPage = GetOddsAsync(Cookie, 1);
                    sw.WriteLine(DateTime.Now);
                    sw.WriteLine(loginPage);

                }
            }
            catch (Exception exception)
            {
                using (StreamWriter sw = System.IO.File.AppendText("D:\\TestFile.txt")) //小寫TXT     
                {
                    sw.WriteLine("*****************");
                    sw.WriteLine("噴錯" + exception.Message);
                    sw.WriteLine("*****************");
                }
            }
            Task.Run(() =>
            {
                var nextTimer = new System.Timers.Timer();
                nextTimer.Interval = 20 * 1000;
                nextTimer.Elapsed += (sender, e) =>
                {
                    TestHeader();
                };
                nextTimer.AutoReset = false;
                nextTimer.Start();
            });
            return null;
        }
        private string Unzip(Stream gZipStream)
        {
            gZipStream = new GZipStream(gZipStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(gZipStream, Encoding.Default);

            string content = Reader.ReadToEnd();

            return content;
        }
        private string Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }
                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }
        private void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }
        private Dictionary<string, string> GetDefaultHeader()
        {
            var headerDic = new Dictionary<string, string>();
            headerDic.Add("Accept-Encoding", "gzip, deflate");
            headerDic.Add("Accept", "*/*");
            headerDic.Add("Referer", "https://xj-sb-asia.prdasbbwla2.com/zh-cn/sports");
            headerDic.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
            return headerDic;
        }
    }

}
