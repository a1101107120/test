﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Extension;
using WebApplication1.Models;
using System.Web;
using Microsoft.AspNetCore.Hosting.Server.Features;
using WebApplication1.Logic;

namespace WebApplication1.Controllers
{
    public class OrderController : Controller
    {
        private readonly IDisbursementLogic _disbursementLogic;

        public OrderController(IDisbursementLogic disbursementLogic)
        {
            _disbursementLogic = disbursementLogic;
        }

        public IActionResult CreateDisbursementOrder()
        {
            
            return View();
        }

        public IActionResult CreatePaymentOrder()
        {
            return View();
        }
        public IActionResult SearchIndex()
        {
            return View();
        }
      
        public IActionResult SubmitDisbursementOrder(CreateDisbursementOrderDto createDto)
        {

            try
            {
                createDto.GivenNullParameter();
            var requestTime = BaseExtensions.GetTimeRequest();
            var sign = $"{nameof(CreateDisbursementOrderDto.MerchantId)}={createDto.MerchantId}&" +
                       $"{nameof(CreateDisbursementOrderDto.MerchantPushDataId)}={createDto.MerchantPushDataId}&" +
                       $"{nameof(CreateDisbursementOrderDto.OrderNo)}={createDto.OrderNo}&" +
                       $"{nameof(CreateDisbursementOrderDto.LoginName)}={createDto.LoginName}&" +
                       $"{nameof(CreateDisbursementOrderDto.BankTypeCode)}={createDto.BankTypeCode}&" +
                       $"{nameof(CreateDisbursementOrderDto.AccountName)}={createDto.AccountName}&" +
                       $"{nameof(CreateDisbursementOrderDto.AccountNo)}={createDto.AccountNo}&" +
                       $"{nameof(CreateDisbursementOrderDto.AccountOpenAddress)}={createDto.AccountOpenAddress}&" +
                       $"{nameof(CreateDisbursementOrderDto.FeeAmount)}={createDto.FeeAmount:#0.00}&" +
                       $"{nameof(CreateDisbursementOrderDto.Amount)}={createDto.Amount:#0.00}&" +
                       $"RequestTime={requestTime}&" +
                       $"ClientIp=127.0.0.1" +
                       $"{createDto.Sign}";

            var encryptSign = sign.ToMD5();

            string url = $"{createDto.LocalIp.Trim()}/api/disbursementOrder";
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Accept = "application/json";
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";



            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add(nameof(CreateDisbursementOrderDto.MerchantId), createDto.MerchantId);
            postParams.Add(nameof(CreateDisbursementOrderDto.MerchantPushDataId),
                createDto.MerchantPushDataId);
            postParams.Add(nameof(CreateDisbursementOrderDto.LoginName), createDto.LoginName);
            postParams.Add(nameof(CreateDisbursementOrderDto.OrderNo), createDto.OrderNo);
            postParams.Add(nameof(CreateDisbursementOrderDto.BankTypeCode), createDto.BankTypeCode);
            postParams.Add(nameof(CreateDisbursementOrderDto.AccountName), createDto.AccountName);
            postParams.Add(nameof(CreateDisbursementOrderDto.AccountNo), createDto.AccountNo);
            postParams.Add(nameof(CreateDisbursementOrderDto.AccountOpenAddress),
                createDto.AccountOpenAddress);
            postParams.Add(nameof(CreateDisbursementOrderDto.Province), createDto.Province);
            postParams.Add(nameof(CreateDisbursementOrderDto.FeeAmount),
                createDto.FeeAmount.ToString("##.###"));
            postParams.Add(nameof(CreateDisbursementOrderDto.Amount), createDto.Amount.ToString("##.###"));
            postParams.Add("RequestTime", requestTime.ToString());
            postParams.Add("ClientIp", "127.0.0.1");
            postParams.Add("Sign", encryptSign);

            byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.WriteAsync(byteArray, 0, byteArray.Length);
            }
            request.ContentLength = byteArray.Length;

                //發出Request
                string responseStr;
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        responseStr = sr.ReadToEnd();
                    }
                }
                //var disbursementOrderResponse = JsonConvert.DeserializeObject<IsSuccessResult>(responseStr);
                return Json(responseStr);
            }
            catch (Exception e)
            {
                var disbursementOrderResponse = new OrderResponse()
                {
                    isSuccess = false,
                    message = "POST API 或 解析JSON發生錯誤"

                };
                var serializeObject = JsonConvert.SerializeObject(disbursementOrderResponse);
                return Json(serializeObject + $"\n{e}");
            }
        }

        public async Task<IActionResult> CreateImpSQL(string infoId, int payType, string impId)
        {
            var result = await _disbursementLogic.CreateImpSQL(infoId, payType, impId);
            if (result.IsSuccess)
            {
                byte[] fileBytes = Encoding.UTF8.GetBytes(result.ReturnObject.Content);
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, result.ReturnObject.FileName);
            }

            return Json(result);

        }
        public async Task<IActionResult> CreateHelpImpSQL(string impHelpId)
        {
            var result = await _disbursementLogic.CreateHelpImpSQL(impHelpId);
            if (result.IsSuccess)
            {
                byte[] fileBytes = Encoding.UTF8.GetBytes(result.ReturnObject.Context);
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, result.ReturnObject.FileName);
            }

            return Json(result);
        }
        public async Task<IActionResult> SearchOtherPayInfo(SearchOtherPayRequest searchOtherPayRequest)
        {
            var result =await _disbursementLogic.SearchOtherPayInfo(searchOtherPayRequest.Status, searchOtherPayRequest.Name);
            return Json(result);
        }

        public IActionResult SubmitPaymentOrder(CreatePaymentOrderDto createDto)
        {
            try
            {
                createDto.GivenNullParameter();
                var sign = $"{nameof(CreatePaymentOrderDto.OrderNo)}={createDto.OrderNo}&" +
                           $"{nameof(CreatePaymentOrderDto.MerchantId)}={createDto.MerchantId}&" +
                           $"{nameof(CreatePaymentOrderDto.LoginName)}={createDto.LoginName}&" +
                           $"{nameof(CreatePaymentOrderDto.Amount)}={createDto.Amount:#0.00}&" +
                           $"{nameof(CreatePaymentOrderDto.PayName)}={createDto.PayName}&" +
                           $"{nameof(CreatePaymentOrderDto.MerchantPushDataId)}={createDto.MerchantPushDataId}&" +
                           $"{nameof(CreatePaymentOrderDto.RequestTime)}={createDto.RequestTime}&" +
                           $"{nameof(CreatePaymentOrderDto.ClientIp)}={createDto.ClientIp}&" +
                           $"{nameof(CreatePaymentOrderDto.MerchantPayInfoId)}={createDto.MerchantPayInfoId}" +
                           $"{createDto.Sign}";

                var encryptSign = sign.ToMD5();

                string url = $"{createDto.LocalIp}/api/order";
                ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.UseDefaultCredentials = true;
                request.PreAuthenticate = true;
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Accept = "application/json";
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";



                NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
                postParams.Add(nameof(CreatePaymentOrderDto.MerchantPushDataId), createDto.MerchantPushDataId);
                postParams.Add(nameof(CreatePaymentOrderDto.MerchantId), createDto.MerchantId);
                postParams.Add(nameof(CreatePaymentOrderDto.MerchantPayInfoId), createDto.MerchantPayInfoId);
                postParams.Add(nameof(CreatePaymentOrderDto.OrderNo), createDto.OrderNo);
                postParams.Add(nameof(CreatePaymentOrderDto.LoginName), createDto.LoginName);
                postParams.Add(nameof(CreatePaymentOrderDto.Amount), createDto.Amount.ToString("##.###"));
                postParams.Add(nameof(CreatePaymentOrderDto.PayName), createDto.PayName);
                postParams.Add(nameof(CreatePaymentOrderDto.RequestTime), createDto.RequestTime);
                postParams.Add(nameof(CreatePaymentOrderDto.ClientIp), createDto.ClientIp);
                postParams.Add(nameof(CreatePaymentOrderDto.Sign), encryptSign);

                byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.WriteAsync(byteArray, 0, byteArray.Length);
                }
                request.ContentLength = byteArray.Length;

                //發出Request
                string responseStr;
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        responseStr = sr.ReadToEnd();
                    }
                }
                //var disbursementOrderResponse = JsonConvert.DeserializeObject<IsSuccessResult>(responseStr);
                return Json(responseStr);
            }
            catch (Exception e)
            {
                var disbursementOrderResponse = new OrderResponse()
                {
                    isSuccess = false,
                    message = "POST API 或 解析JSON發生錯誤"

                };
                var serializeObject = JsonConvert.SerializeObject(disbursementOrderResponse);
                return Json(serializeObject + $"\n{e}");
            }
        }
    }
}