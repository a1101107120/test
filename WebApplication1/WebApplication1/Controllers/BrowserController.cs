﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    public class BrowserController : Controller
    {
        private IConfiguration _configuration;
        public BrowserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
           
            return View();
        }


        public async Task InitBrowser()
        {
            //await new BrowserService().InitBrowser();
            new BrowserService(_configuration).GetStockData().GetAwaiter();
        }
    }
}
