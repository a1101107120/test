﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using WebApplication1.Service;
using WebApplication1.Service.Interface;

namespace WebApplication1.Controllers
{
    public class RabbitMqController : Controller
    {
        public IActionResult PushMQIndex()
        {
            return View();
        }

        public IActionResult PushDisbursementOrder(MqEventCreateDto eventDto)
        {
            try
            {
                ConnectionFactory factory = new ConnectionFactory
                {
                    HostName = eventDto.MQIP,
                    UserName = "guest",
                    Password = "guest",
                    VirtualHost = "/",
                    Port = 5672,
                };
                var mqConnection = new DefaultRabbitMQPersistentConnection(factory);
                var eventBus = new EventBusRabbitMq(mqConnection);

                if (eventDto.Type == 0)
                {
                    eventBus.Publish(new CreateDisbursementOrderIntegrationEvent()
                    {
                        DisbursementOrderId = eventDto.DisbursementOrderId,
                        MerchantId = eventDto.MerchantId
                    });
                }
                else if (eventDto.Type == 1)
                {
                    eventBus.Publish(new QueryDisbursementOrderIntegrationEvent()
                    {
                        DisbursementOrderId = eventDto.DisbursementOrderId,
                        MerchantId = eventDto.MerchantId
                    });
                }
                else
                {
                    return Json("MQ推送失敗，不明的Routing key");
                }

                mqConnection.Dispose();
                return Json("MqEventCreateDto 推送成功");
            }
            catch (Exception e)
            {
                return Json($"MqEventCreateDto 發生例外,{e}");
            }
        }
    }

    public class MqEventCreateDto
    {
        [Required(ErrorMessage = "Mq服務IP必填")]
        public string MQIP { get; set; }
        public string DisbursementOrderId { get; set; }
        public string MerchantId { get; set; }
        /// <summary>
        /// 0:CreateDisbursementOrderIntegrationEvent
        /// 1:QueryDisbursementOrderIntegrationEvent
        /// </summary>
        public int Type { get; set; }
    }

    public class CreateDisbursementOrderIntegrationEvent
    {
        public string DisbursementOrderId { get; set; }
        public string MerchantId { get; set; }
    }
    public class QueryDisbursementOrderIntegrationEvent
    {
        public string DisbursementOrderId { get; set; }
        public string MerchantId { get; set; }
    }
}