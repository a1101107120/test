﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Library.DbContext;
using Library.DbModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop.Infrastructure;
using WebApplication1.Extension;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IConfiguration _configuration;

        private ZdCapitalOrderContext _capitalOrderContext;
        public HomeController(ILogger<HomeController> logger, ZdCapitalOrderContext capitalOrderContext, IConfiguration configuration)
        {
            _logger = logger;
            _capitalOrderContext = capitalOrderContext;
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> PressTest()
        {
            var zdCapitalOrderContext = new ZdCapitalOrderContext(_capitalOrderContext.Options);
            await Task.Run(() =>
            {
                var count = 1000000;
                var index = 1;
                var merchantPushData = new Dictionary<int, (string MerchantPushDataId, string MerchantId)>();
                merchantPushData = zdCapitalOrderContext.MerchantPushData
                    .ToDictionary(r => r.Id, r => r.MerchantId)
                    .OrderBy(r => r.Key)
                    .Select((dto, index) => new
                    {
                        index,
                        dto.Key,
                        dto.Value
                    }).ToDictionary(r => r.index, r => (r.Key, r.Value));
                var payInfo = zdCapitalOrderContext.MerchantPayInfo.ToList();
                var rechargeMod = zdCapitalOrderContext.RechargeMode.ToList();

                try
                {
                    Parallel.For(0, 20, (i, loopState) =>
                    {
                        var addList = new List<MerchantTransferOrder>();
                        for (int j = 0; j < count; j++)
                        {
                            var random = new Random();
                            var merchantData = merchantPushData[random.Next(merchantPushData.Count)];
                            var payInfoData = payInfo[random.Next(payInfo.Count)];
                            var rechargeModData = rechargeMod[random.Next(rechargeMod.Count)];
                            var dto = new MerchantTransferOrder()
                            {
                                Id = Guid.NewGuid().ToString("N"),
                                Date = DateTime.Now.Date,
                                MerchantId = merchantData.MerchantId,
                                MerchantPushDataId = merchantData.MerchantPushDataId,
                                Amount = random.Next(0, 1000),
                                OtherPayInfoId = payInfoData.OtherPayInfoId,
                                OtherPayImpId = payInfoData.OtherPayImpId,
                                MerchantPayInfoId = payInfoData.Id,
                                IncomeCodeId = string.Empty,
                                BankcardId = string.IsNullOrEmpty(payInfoData.BankcardId) ? "" : payInfoData.BankcardId,
                                BankcardNo = 123456.ToString(),
                                BankcardAccount = "Test",
                                RechargeModeId = rechargeModData.Id,
                                MerchantOrderNo = Guid.NewGuid().ToString("N"),
                                OtherOrderNo = Guid.NewGuid().ToString("N"),
                                LoginName = "TEST",
                                PayName = "TEST",
                                Postscript = string.Empty,
                                RealAmount = random.Next(0, 1000),
                                BankPayName = string.Empty,
                                BankPostscript = string.Empty,
                                ClientIp = 3232248441,
                                Status = byte.Parse(random.Next(1, 4).ToString()),

                            };
                            addList.Add(dto);
                            if (j % 100 == 0)
                            {
                                using var zdContext = new ZdCapitalOrderContext(_capitalOrderContext.Options);
                                zdContext.MerchantTransferOrder.AddRange(addList);
                                zdContext.SaveChanges();
                                addList.Clear();
                            }
                        }

                    });
                }
                catch (Exception e)
                {

                }

            });

            return View("Privacy");
        }

        public IActionResult PressTestMerchantOrder()
        {
            var count = 1000000;
            var index = 1;
            var merchantPushData = new Dictionary<int,(string MerchantPushDataId, string MerchantId)>();
            merchantPushData = _capitalOrderContext.MerchantPushData
                .ToDictionary(r => r.Id, r => r.MerchantId)
                .OrderBy(r => r.Key)
                .Select((dto, index) => new
                {
                    index,
                    dto.Key,
                    dto.Value
                }).ToDictionary(r => r.index, r => (r.Key, r.Value));
            var payInfo = _capitalOrderContext.MerchantPayInfo.ToList();
            var rechargeMod = _capitalOrderContext.RechargeMode.ToList();

            try
            {
                Parallel.For(0, 20, (i, loopState) =>
                {
                    var addList = new List<MerchantOrder>();
                    for (int j = 0; j < count; j++)
                    {
                        var random = new Random();
                        var merchantData = merchantPushData[random.Next(merchantPushData.Count)];
                        var payInfoData = payInfo[random.Next(payInfo.Count)];
                        var rechargeModData = rechargeMod[random.Next(rechargeMod.Count)];
                        var dto = new MerchantOrder()
                        {
                            Id = Guid.NewGuid().ToString("N"),
                            Date = DateTime.Now.Date,
                            MerchantId = merchantData.MerchantId,
                            MerchantPushDataId = merchantData.MerchantPushDataId,
                            Amount = random.Next(0, 1000),
                            OtherPayInfoId = payInfoData.OtherPayInfoId,
                            OtherPayImpId = payInfoData.OtherPayImpId,
                            MerchantPayInfoId = payInfoData.Id,
                            BankcardId = string.IsNullOrEmpty(payInfoData.BankcardId) ? "" : payInfoData.BankcardId,
                            RechargeModeId = rechargeModData.Id,
                            MerchantOrderNo = Guid.NewGuid().ToString("N"),
                            LoginName = "TEST",
                            RealAmount = random.Next(0, 1000),
                            ClientIp = 3232248441,
                            Status = byte.Parse(random.Next(1, 4).ToString()),
                            IsDelete = 0,
                            OtherPayOrderNo = Guid.NewGuid().ToString("N"),

                        };
                        addList.Add(dto);
                        if (j % 100 == 0)
                        {
                            using var zdContext = new ZdCapitalOrderContext(_capitalOrderContext.Options);
                            zdContext.MerchantOrder.AddRange(addList);
                            zdContext.SaveChanges();
                            addList.Clear();
                        }
                    }

                });
            }
            catch (Exception e)
            {

            }
            

            return Json("Success");
        }

        public IActionResult Test()
        {
            var startDate = new DateTime(2020,11,10,12,0,0);
            var endDate = new DateTime(2020,11,10,23,59,59);
            var list = (from mto in _capitalOrderContext.MerchantTransferOrder
                join oImp in _capitalOrderContext.OtherPayImp on mto.OtherPayImpId equals oImp.Id
                join otherPayInfo in _capitalOrderContext.OtherPayInfo on mto.OtherPayInfoId equals otherPayInfo.Id
                where (mto.MerchantId == "29a7d44f72fc414a861fc578fc9e19cf" && mto.RowTime > startDate &&
                       mto.RowTime < endDate)
                select new
                {
                    OtherPayImpId = mto.OtherPayImpId,
                    BName = oImp.Name,
                    CName = otherPayInfo.Name,
                    BankTypeName = "-",
                    Mode = "线上支付",
                    Account = "-",
                }).AsNoTracking().ToList();


            var sss = list.Count;
            return View("Privacy");
        }

        public IActionResult TestConnect()
        {
            var connection1 = _configuration["AppSettings:ZdCapitalOrderConnectionString"];
            var connection2 = _configuration["AppSettings:FakeOtherPayConnectionString"];

            return Json(new
            {
                ZdCapitalOrderConnectionString = connection1,
                FakeOtherPayConnectionString = connection2,
                EnvironmentDic = System.Environment.CurrentDirectory,
                AppDomain = System.AppDomain.CurrentDomain.BaseDirectory

            });
        }
    }
}
