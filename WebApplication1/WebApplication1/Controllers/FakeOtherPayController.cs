﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Library.FakeOtherPayDbModel;

namespace WebApplication1.Controllers
{
    public class FakeOtherPayController : Controller
    {
        private readonly FakeOtherPayContext _context;

        public FakeOtherPayController(FakeOtherPayContext context)
        {
            _context = context;
        }

        // GET: FakeOtherPay
        public async Task<IActionResult> Index()
        {
            return View(await _context.OtherPay.ToListAsync());
        }

        // GET: FakeOtherPay/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var otherPay = await _context.OtherPay
                .FirstOrDefaultAsync(m => m.OtherPayCode == id);
            if (otherPay == null)
            {
                return NotFound();
            }

            return View(otherPay);
        }

        // GET: FakeOtherPay/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FakeOtherPay/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OtherPayCode,Name,Type,ResponseData")] OtherPay otherPay)
        {
            if (ModelState.IsValid)
            {
                _context.Add(otherPay);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(otherPay);
        }

        // GET: FakeOtherPay/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var otherPay = await _context.OtherPay.FindAsync(id);
            if (otherPay == null)
            {
                return NotFound();
            }
            return View(otherPay);
        }

        // POST: FakeOtherPay/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("OtherPayCode,Name,Type,ResponseData")] OtherPay otherPay)
        {
            if (id != otherPay.OtherPayCode)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(otherPay);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OtherPayExists(otherPay.OtherPayCode))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(otherPay);
        }

        // GET: FakeOtherPay/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var otherPay = await _context.OtherPay
                .FirstOrDefaultAsync(m => m.OtherPayCode == id);
            if (otherPay == null)
            {
                return NotFound();
            }

            return View(otherPay);
        }

        // POST: FakeOtherPay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var otherPay = await _context.OtherPay.FindAsync(id);
            _context.OtherPay.Remove(otherPay);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OtherPayExists(string id)
        {
            return _context.OtherPay.Any(e => e.OtherPayCode == id);
        }
    }
}
