﻿using System;

namespace WebApplication1.Extension
{
    public static class BaseExtensions
    {
        // 取得亂數
        public static string GetRandomOrderNo()
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            int passwordLength = 15;//密碼長度
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                //allowedChars -> 這個String ，隨機取得一個字，丟給chars[i]
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            var randomString = new string(chars);
            return randomString;
        }
        //取得時間戳記
        public static int GetTimeRequest()
        {
            return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}