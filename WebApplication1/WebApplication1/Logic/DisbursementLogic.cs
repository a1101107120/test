﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Bond.IO.Unsafe;
using Library.DbContext;
using Library.DbModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Controllers;
using WebApplication1.Models;
using OtherPayImp = WebApplication1.Models.OtherPayImp;

namespace WebApplication1.Logic
{
    public interface IDisbursementLogic
    {
        Task<IsSuccessResult<OtherPayViewModel>> SearchOtherPayInfo(int? status, string name);
        Task<IsSuccessResult<(string Content, string FileName)>> CreateImpSQL(string infoId, int payType, string impId);
        Task<IsSuccessResult<List<OtherPayInfoDto>>> SearchOtherPayHelpImp(int? status, string name);
        Task<IsSuccessResult<(string Context, string FileName)>> CreateHelpImpSQL(string impHelpId);
    }
    public class DisbursementLogic : IDisbursementLogic
    {
        public readonly ZdCapitalOrderContext ZdCapitalOrderContext;

        public DisbursementLogic(ZdCapitalOrderContext zdCapitalOrderContext)
        {
            ZdCapitalOrderContext = zdCapitalOrderContext;
        }


        public async Task<IsSuccessResult<OtherPayViewModel>> SearchOtherPayInfo(int? status, string name)
        {
            try
            {
                var query = ZdCapitalOrderContext.OtherPayInfo.AsQueryable().Where(r=>r.IsDelete == 0);
                var otherPayHelpImpList = new List<OtherPayHelpImp>();
                if (string.IsNullOrWhiteSpace(name) == false)
                {
                    query = query.Where(r => r.Name.Contains(name));
                    otherPayHelpImpList = await ZdCapitalOrderContext.OtherPayHelperImp
                        .Where(r => r.Name.Contains(name) && r.IsDelete == 0).Select(r => new OtherPayHelpImp()
                        {
                            Id = r.Id,
                            Name = r.Name,
                            Description = r.DescriptionContent,
                            Namespace = r.Namespace,
                            ImpClass = r.ImpClass,

                        }).ToListAsync();

                }

                if (status.HasValue)
                {
                    query = query.Where(r => r.PayType == status);
                }

                var otherPayInfoList = await query.Select(r => new OtherPayInfoDto()
                {
                    Id = r.Id,
                    Code = r.Code,
                    Name = r.Name,
                    PayType = r.PayType
                }).ToListAsync();

                var otherPayImps = await ZdCapitalOrderContext.OtherPayImp.ToListAsync();
                var otherDisbursementImps = await ZdCapitalOrderContext.OtherDisbursementImp.ToListAsync();

                foreach (var otherPayInfoDto in otherPayInfoList)
                {
                    if (otherPayInfoDto.PayType == 0)
                    {
                        otherPayInfoDto.OtherPayImpList = otherPayImps
                            .Where(r => r.OtherPayInfoId == otherPayInfoDto.Id && r.IsDelete == 0)
                            .Select(r => new OtherPayImp()
                            {
                                Id = r.Id,
                                Name = r.Name,
                                Description = r.DescriptionContent

                            }).ToList();
                    }
                    else if (otherPayInfoDto.PayType == 1)
                    {
                        otherPayInfoDto.OtherPayImpList = otherDisbursementImps
                            .Where(r => r.OtherPayInfoId == otherPayInfoDto.Id && r.IsDelete == 0)
                            .Select(r => new OtherPayImp()
                            {
                                Id = r.Id,
                                Name = r.Name,
                                Description = r.DescriptionContent

                            }).ToList();

                    }
                    else
                    {
                        otherPayInfoDto.OtherPayImpList = new List<OtherPayImp>();
                    }
                }
                var result = new OtherPayViewModel
                {
                    OtherPayInfoList = otherPayInfoList,
                    OtherPayHelpImpList = otherPayHelpImpList
                };

                return new IsSuccessResult<OtherPayViewModel>()
                {
                    ReturnObject = result
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IsSuccessResult<(string Content, string FileName)>> CreateImpSQL(string infoId, int payType, string impId)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                var now = $"{DateTime.Now:yyyy-MM-dd HH:MM:ss}";
                var timestamp = $"{DateTime.Now:HHMMss}";
                var name = "SQL檔";
                //支付
                if (payType == 0)
                {
                    var otherPayImp = await ZdCapitalOrderContext.OtherPayImp.FirstOrDefaultAsync(r => r.Id == impId&&r.IsDelete == 0);
                    if (otherPayImp != null)
                    {

                        var otherPayImpSql = GetSqlContents(otherPayImp);
                        stringBuilder.Append(otherPayImpSql);

                        ////換行符號取代            
                        //otherPayImp.CallbackJavascript = otherPayImp.CallbackJavascript.Replace("\r", @"\r").Replace("\n", @"\n").Replace("'", @"\'");
                        //otherPayImp.RequestReturnJavascript = otherPayImp.RequestReturnJavascript.Replace("\r", @"\r").Replace("\n", @"\n").Replace("'", @"\'");

                        ////可為NULL的值 在Append會少''符號
                        //otherPayImp.InterfaceVersion = string.IsNullOrEmpty(otherPayImp.InterfaceVersion) ? "NULL" : $"'{otherPayImp.InterfaceVersion}'";
                        //otherPayImp.ParameterType = string.IsNullOrEmpty(otherPayImp.ParameterType) ? "NULL" : $"'{otherPayImp.ParameterType}'";
                        //otherPayImp.SignFixedParameter = string.IsNullOrEmpty(otherPayImp.SignFixedParameter) ? "NULL" : $"'{otherPayImp.SignFixedParameter}'";
                        //otherPayImp.QuerySignFixedParameter = string.IsNullOrEmpty(otherPayImp.QuerySignFixedParameter) ? "NULL" : $"'{otherPayImp.QuerySignFixedParameter}'";
                        //otherPayImp.EncryptionHelperId = string.IsNullOrEmpty(otherPayImp.EncryptionHelperId) ? "NULL" : $"'{otherPayImp.EncryptionHelperId}'";
                        //otherPayImp.SignHelperId = string.IsNullOrEmpty(otherPayImp.SignHelperId) ? "NULL" : $"'{otherPayImp.SignHelperId}'";
                        //otherPayImp.RequestReturnHelperId = string.IsNullOrEmpty(otherPayImp.RequestReturnHelperId) ? "NULL" : $"'{otherPayImp.RequestReturnHelperId}'";
                        //otherPayImp.OrderNoHelperId = string.IsNullOrEmpty(otherPayImp.OrderNoHelperId) ? "NULL" : $"'{otherPayImp.OrderNoHelperId}'";
                        //otherPayImp.SignEndHelperId = string.IsNullOrEmpty(otherPayImp.SignEndHelperId) ? "NULL" : $"'{otherPayImp.SignEndHelperId}'";
                        //otherPayImp.OrderCallbackHelperId = string.IsNullOrEmpty(otherPayImp.OrderCallbackHelperId) ? "NULL" : $"'{otherPayImp.OrderCallbackHelperId}'";
                        //otherPayImp.CallbackJavascript = string.IsNullOrEmpty(otherPayImp.CallbackJavascript) ? "NULL" : $"'{otherPayImp.CallbackJavascript}'";
                        //otherPayImp.RequestReturnJavascript = string.IsNullOrEmpty(otherPayImp.RequestReturnJavascript) ? "NULL" : $"'{otherPayImp.RequestReturnJavascript}'";
                        //otherPayImp.PayType = string.IsNullOrEmpty(otherPayImp.PayType) ? "NULL" : $"'{otherPayImp.PayType}'";
                        //otherPayImp.AmountFixedParameter = string.IsNullOrEmpty(otherPayImp.AmountFixedParameter) ? "NULL" : $"'{otherPayImp.AmountFixedParameter}'";
                        //otherPayImp.ShowCallbackController = string.IsNullOrEmpty(otherPayImp.ShowCallbackController) ? "NULL" : $"'{otherPayImp.ShowCallbackController}'";
                        //otherPayImp.CurrencyTypeId = string.IsNullOrEmpty(otherPayImp.CurrencyTypeId) ? "NULL" : $"'{otherPayImp.CurrencyTypeId}'";

                        //stringBuilder.Append(@$"INSERT INTO `OtherPayImp` (`Id`, `OtherPayInfoId`, `RechargeModeId`, `Name`, `DescriptionContent`, `SignName`, `InterfaceVersion`, `Encoding`, `ParameterType`, `SignFixedParameter`, `QuerySignFixedParameter`, `EncryptionHelperId`, `SignHelperId`, `RequestReturnHelperId`, `OrderNoHelperId`, `SignEndHelperId`, `CreateOrderHelperId`, `OrderCallbackHelperId`, `CallbackJavascript`, `RequestReturnJavascript`, `PayType`, `MinLoad`, `MaxLoad`, `LimitAmountType`, `AmountFixedParameter`, `Sequence`, `RequestMode`, `QueryMode`, `PollingType`, `IsRandomAmount`, `IsOpenPostscript`, `IsShowCallback`, `IsNotifyCustomerService`, `ShowCallbackController`, `ApplicationScope`, `PostscriptLength`, `MatchingTime`, `Mode`, `Status`, `RequirePayName`, `CreateOrderAddress`, `QueryOrderAddress`, `QueryAmountAddress`, `IsDelete`, `CreateTime`, `RowTime`, `CurrencyTypeId`) VALUES ('{otherPayImp.Id}', '{otherPayImp.OtherPayInfoId}', '{otherPayImp.RechargeModeId}', '{otherPayImp.Name}', '{otherPayImp.DescriptionContent}', '{otherPayImp.SignName}', {otherPayImp.InterfaceVersion}, '{otherPayImp.Encoding}', {otherPayImp.ParameterType}, {otherPayImp.SignFixedParameter}, {otherPayImp.QuerySignFixedParameter}, {otherPayImp.EncryptionHelperId}, {otherPayImp.SignHelperId}, {otherPayImp.RequestReturnHelperId}, {otherPayImp.OrderNoHelperId}, {otherPayImp.SignEndHelperId}, '{otherPayImp.CreateOrderHelperId}', {otherPayImp.OrderCallbackHelperId}, {otherPayImp.CallbackJavascript}, {otherPayImp.RequestReturnJavascript}, {otherPayImp.PayType}, {otherPayImp.MinLoad}, {otherPayImp.MaxLoad}, {otherPayImp.LimitAmountType}, {otherPayImp.AmountFixedParameter}, {otherPayImp.Sequence}, {otherPayImp.RequestMode}, {otherPayImp.QueryMode}, {otherPayImp.PollingType}, {otherPayImp.IsRandomAmount}, {otherPayImp.IsOpenPostscript}, {otherPayImp.IsShowCallback}, {otherPayImp.IsNotifyCustomerService}, {otherPayImp.ShowCallbackController}, {otherPayImp.ApplicationScope}, {otherPayImp.PostscriptLength}, {otherPayImp.MatchingTime}, {otherPayImp.Mode}, {otherPayImp.Status}, {otherPayImp.RequirePayName}, '{otherPayImp.CreateOrderAddress}', '{otherPayImp.QueryOrderAddress}', '{otherPayImp.QueryAmountAddress}', {otherPayImp.IsDelete}, '{now}', '{now}', {otherPayImp.CurrencyTypeId});");
                        //stringBuilder.AppendLine();
                    }
                }
                else if (payType == 1)
                {
                    var otherDisbursementImp = await ZdCapitalOrderContext.OtherDisbursementImp.FirstOrDefaultAsync(r => r.Id == impId && r.IsDelete == 0);
                    if (otherDisbursementImp != null)
                    {
                        var otherDisbursementImpSql = GetSqlContents(otherDisbursementImp);
                        stringBuilder.Append(otherDisbursementImpSql);


                        ////換行符號取代            
                        //otherDisbursementImp.RequestReturnJavascript = otherDisbursementImp.RequestReturnJavascript.Replace("\r", @"\r").Replace("\n", @"\n").Replace("'", @"\'");
                        //otherDisbursementImp.CallbackJavascript = otherDisbursementImp.CallbackJavascript.Replace("\r", @"\r").Replace("\n", @"\n").Replace("'", @"\'");
                        //if (string.IsNullOrWhiteSpace(otherDisbursementImp.QueryAmountJavascript) == false)
                        //{
                        //    otherDisbursementImp.QueryAmountJavascript = otherDisbursementImp.QueryAmountJavascript.Replace("\r", @"\r").Replace("\n", @"\n").Replace("'", @"\'");
                        //}

                        ////可為NULL的值
                        //otherDisbursementImp.SignFixedParameter =string.IsNullOrEmpty(otherDisbursementImp.SignFixedParameter) ? "NULL" : $"'{otherDisbursementImp.SignFixedParameter}'";
                        //otherDisbursementImp.QuerySignFixedParameter =string.IsNullOrEmpty(otherDisbursementImp.QuerySignFixedParameter) ? "NULL" : $"'{otherDisbursementImp.QuerySignFixedParameter}'";
                        //otherDisbursementImp.QueryAmountSignFixedParameter =string.IsNullOrEmpty(otherDisbursementImp.QueryAmountSignFixedParameter) ? "NULL" : $"'{otherDisbursementImp.QueryAmountSignFixedParameter}'";
                        //otherDisbursementImp.SignEndHelperId =string.IsNullOrEmpty(otherDisbursementImp.SignEndHelperId) ? "NULL" : $"'{otherDisbursementImp.SignEndHelperId}'";
                        //otherDisbursementImp.QueryAmountHelperId = string.IsNullOrEmpty(otherDisbursementImp.QueryAmountHelperId) ? "NULL" : $"'{otherDisbursementImp.QueryAmountHelperId}'";
                        //otherDisbursementImp.QueryAmountJavascript = string.IsNullOrEmpty(otherDisbursementImp.QueryAmountJavascript) ? "NULL" : $"'{otherDisbursementImp.QueryAmountJavascript}'";

                    
                        //stringBuilder.Append($@"INSERT INTO `OtherDisbursementImp` (`Id`, `OtherPayInfoId`, `Name`, `DescriptionContent`, `SignName`, `Encoding`, `ParameterType`, `SignFixedParameter`, `QuerySignFixedParameter`, `QueryAmountSignFixedParameter`, `EncryptionHelperId`, `SignHelperId`, `OrderNoHelperId`, `SignEndHelperId`, `CreateOrderHelperId`, `OrderCallbackHelperId`, `QueryAmountHelperId`, `RequestReturnJavascript`, `CallbackJavascript`, `QueryAmountJavascript`, `MinLoad`, `MaxLoad`, `Sequence`, `RequestMode`, `QueryMode`, `QueryAmount`, `Status`, `CreateOrderAddress`, `QueryOrderAddress`, `QueryAmountAddress`, `IsDelete`, `CreateTime`, `RowTime`) VALUES ('{otherDisbursementImp.Id}', '{otherDisbursementImp.OtherPayInfoId}', '{otherDisbursementImp.Name}', '{otherDisbursementImp.DescriptionContent}', '{otherDisbursementImp.SignName}', '{otherDisbursementImp.Encoding}', '{otherDisbursementImp.ParameterType}', {otherDisbursementImp.SignFixedParameter}, {otherDisbursementImp.QuerySignFixedParameter}, {otherDisbursementImp.QueryAmountSignFixedParameter}, '{otherDisbursementImp.EncryptionHelperId}', '{otherDisbursementImp.SignHelperId}', '{otherDisbursementImp.OrderNoHelperId}', {otherDisbursementImp.SignEndHelperId}, '{otherDisbursementImp.CreateOrderHelperId}', '{otherDisbursementImp.OrderCallbackHelperId}', {otherDisbursementImp.QueryAmountHelperId}, '{otherDisbursementImp.RequestReturnJavascript}', '{otherDisbursementImp.CallbackJavascript}',{otherDisbursementImp.QueryAmountJavascript}, {otherDisbursementImp.MinLoad}, {otherDisbursementImp.MaxLoad}, {otherDisbursementImp.Sequence}, {otherDisbursementImp.RequestMode}, {otherDisbursementImp.QueryMode}, {otherDisbursementImp.QueryAmount}, {otherDisbursementImp.Status}, '{otherDisbursementImp.CreateOrderAddress}', '{otherDisbursementImp.QueryOrderAddress}', '{otherDisbursementImp.QueryOrderAddress}', {otherDisbursementImp.IsDelete}, '{now}', '{now}');");
                        //stringBuilder.AppendLine();
                    }
                }
                stringBuilder.AppendLine();


                // 印出OtherPayInfo
                var otherPayInfo =
                    await ZdCapitalOrderContext.OtherPayInfo.Where(r => r.Id == infoId && r.IsDelete == 0).FirstOrDefaultAsync();
                if (otherPayInfo!= null)
                {
                    name = otherPayInfo.Name;
                    var otherPayInfoSql = GetSqlContents(otherPayInfo);
                    stringBuilder.Append(otherPayInfoSql);
                    //stringBuilder.Append(
                    //    $@"INSERT INTO `OtherPayInfo` (`Id`, `Code`, `Name`, `Sequence`, `Status`, `PayType`, `IsDelete`, `CreateTime`, `RowTime`) VALUES ('{otherPayInfo.Id}', '{otherPayInfo.Code}', '{otherPayInfo.Name}', {otherPayInfo.Sequence}, {otherPayInfo.Status}, {otherPayInfo.PayType}, {otherPayInfo.IsDelete}, '{now}', '{now}');");
                    //stringBuilder.AppendLine();
                }
                stringBuilder.AppendLine();

                // 印出OtherPayRequireParameter
                var otherPayRpList = await ZdCapitalOrderContext.OtherPayRequireParameter
                    .Where(r => r.OtherPayInfoId == infoId && r.PayType == payType && r.IsDelete == 0).ToListAsync();
                foreach (var item in otherPayRpList)
                {

                    var otherPayRpSql = GetSqlContents(item);
                    stringBuilder.Append(otherPayRpSql);
                    //stringBuilder.Append(
                    //    $@"INSERT INTO `OtherPayRequireParameter` (`Id`, `ParameterName`, `ShowName`, `OtherPayInfoId`, `PayType`, `Sequence`, `IsDelete`, `CreateTime`, `RowTime`) VALUES ('{item.Id}', '{item.ParameterName}', '{item.ShowName}', '{item.OtherPayInfoId}', {item.PayType}, {item.Sequence}, {item.IsDelete}, '{now}', '{now}');");
                    //stringBuilder.AppendLine();
                }
                stringBuilder.AppendLine();
                // 印出OtherPayImpRequestParameterMaps
                var impParameter 
                    = await ZdCapitalOrderContext.OtherPayImpRequestParameterMaps
                        .Where(r => r.OtherPayInfoId == infoId && r.PayType == payType && r.OtherPayImpId == impId && r.IsDelete == 0)
                        .ToListAsync();
                foreach (var item in impParameter)
                {

                    var impParameterSql = GetSqlContents(item);
                    stringBuilder.Append(impParameterSql);
                    //stringBuilder.Append(
                    //    $@"INSERT INTO `OtherPayImpRequestParameterMaps` (`Id`, `OtherPayInfoId`, `OtherPayImpId`, `ParameterName`, `ParameterMapTo`, `ScopeType`, `PayType`, `Value`, `Sequence`, `IsDelete`, `CreateTime`, `RowTime`) VALUES ('{item.Id}', '{item.OtherPayInfoId}', '{item.OtherPayImpId}', '{item.ParameterName}', '{item.ParameterMapTo}', {item.ScopeType}, {item.PayType}, {(string.IsNullOrWhiteSpace(item.Value) ? "NULL" : $"'{item.Value}'")}, {item.Sequence}, {item.IsDelete}, '{now}', '{now}');");
                    //stringBuilder.AppendLine();
                }
                stringBuilder.AppendLine();

                // 印出OtherPayBankTypeMaps
                var bankMaps = await ZdCapitalOrderContext.OtherPayBankTypeMaps
                    .Where(r => r.OtherPayImpId == impId && r.PayType == payType && r.IsDelete == 0).ToListAsync();
                foreach (var item in bankMaps)
                {

                    var bankMapSql = GetSqlContents(item);
                    stringBuilder.Append(bankMapSql);
                    //stringBuilder.Append(
                    //    $@"INSERT INTO `OtherPayBankTypeMaps` (`Id`, `BankTypeId`, `OtherPayImpId`, `OtherPayBankTypeCode`, `OtherPayBankTypeName`, `PayType`, `Sequence`, `Status`, `IsDelete`, `CreateTime`, `RowTime`) VALUES ('{item.Id}', '{item.BankTypeId}', '{item.OtherPayImpId}', '{item.OtherPayBankTypeCode}', '{item.OtherPayBankTypeName}', {item.PayType}, {item.Sequence}, {item.Status}, {item.IsDelete}, '{now}', '{now}');");
                    //stringBuilder.AppendLine();
                }
                stringBuilder.AppendLine();

                var contents = stringBuilder.ToString();

              return new IsSuccessResult<(string Content, string fileName)>()
              {
                  ReturnObject = (contents,$"{name}{timestamp}.txt")
              };
            }
            catch (Exception e)
            {
                return new IsSuccessResult<(string Content, string fileName)>(e.ToString());
            }
        }

        public async Task<IsSuccessResult<List<OtherPayInfoDto>>> SearchOtherPayHelpImp(int? status, string name)
        {

            var query = ZdCapitalOrderContext.OtherPayHelperImp.AsQueryable().Where(r=>r.IsDelete == 0);
            if (string.IsNullOrWhiteSpace(name) == false)
            {
                query = query.Where(r => r.Name.Contains(name));
            }

            if (status.HasValue)
            {
                query = query.Where(r => r.PayType == status);
            }

            var result = await query.Select(r => new OtherPayInfoDto()
            {
                Id = r.Id,
                Name = r.Name,
                PayType = r.PayType
            }).ToListAsync();

            var otherPayImps = await ZdCapitalOrderContext.OtherPayImp.ToListAsync();
            var otherDisbursementImps = await ZdCapitalOrderContext.OtherDisbursementImp.ToListAsync();

            foreach (var otherPayInfoDto in result)
            {
                if (otherPayInfoDto.PayType == 0)
                {
                    otherPayInfoDto.OtherPayImpList = otherPayImps
                        .Where(r => r.OtherPayInfoId == otherPayInfoDto.Id && r.IsDelete == 0)
                        .Select(r => new OtherPayImp()
                        {
                            Id = r.Id,
                            Name = r.Name,
                            Description = r.DescriptionContent

                        }).ToList();
                }
                else if (otherPayInfoDto.PayType == 1)
                {
                    otherPayInfoDto.OtherPayImpList = otherDisbursementImps
                        .Where(r => r.OtherPayInfoId == otherPayInfoDto.Id && r.IsDelete == 0)
                        .Select(r => new OtherPayImp()
                        {
                            Id = r.Id,
                            Name = r.Name,
                            Description = r.DescriptionContent

                        }).ToList();

                }
                else
                {
                    otherPayInfoDto.OtherPayImpList = new List<OtherPayImp>();
                }
            }

            return new IsSuccessResult<List<OtherPayInfoDto>>()
            {
                ReturnObject = result
            };
        }

        public async Task<IsSuccessResult<(string Context, string FileName)>> CreateHelpImpSQL(string impHelpId)
        {
            //try
            //{
            //    var stringBuilder = new StringBuilder();
            //    var now = $"{DateTime.Now:yyyy-MM-dd HH:MM:ss}";
            //    var timestamp = $"{DateTime.Now:HHMMss}";
            //    var name = "SQL檔";

            //    // 印出OtherPayInfo
            //    var otherPayHelperImp =
            //        await ZdCapitalOrderContext.OtherPayHelperImp.Where(r => r.Id == impHelpId).FirstOrDefaultAsync();
            //    if (otherPayHelperImp != null)
            //    {
            //        name = otherPayHelperImp.Name;
            //        stringBuilder.Append(
            //            $@"INSERT INTO `OtherPayHelperImp` (`Id`, `Name`, `DescriptionContent`, `Assembly`, `Namespace`, `ImpClass`, `Type`, `PayType`, `Sequence`, `Status`, `IsDelete`, `CreateTime`, `RowTime`) VALUES ('{otherPayHelperImp.Id}', '{otherPayHelperImp.Name}', '{otherPayHelperImp.DescriptionContent}', '{otherPayHelperImp.Assembly}', '{otherPayHelperImp.Namespace}', '{otherPayHelperImp.ImpClass}', {otherPayHelperImp.Type}, {otherPayHelperImp.PayType}, {otherPayHelperImp.Sequence}, {otherPayHelperImp.Status}, {otherPayHelperImp.IsDelete}, '{now}', '{now}');");

            //        stringBuilder.AppendLine();
            //    }
            //    stringBuilder.AppendLine();
            //    var contents = stringBuilder.ToString();
            //    return new IsSuccessResult<(string Context, string FileName)>()
            //    {
            //        ReturnObject = (contents, $"{name}{timestamp}.txt")
            //    };
            //}
            //catch (Exception e)
            //{
            //    return new IsSuccessResult<(string Context, string FileName)>(e.ToString());
            //}

            try
            {
                var stringBuilder = new StringBuilder();
                var timestamp = $"{DateTime.Now:HHMMss}";
                var name = "SQL檔";

                // 印出OtherPayInfo
                var otherPayHelperImp =
                    await ZdCapitalOrderContext.OtherPayHelperImp.Where(r => r.Id == impHelpId && r.IsDelete == 0).FirstOrDefaultAsync();
                if (otherPayHelperImp != null)
                {
                    name = otherPayHelperImp.Name;
                    var sqlContents = GetSqlContents(otherPayHelperImp);
                    stringBuilder.AppendLine(sqlContents);
                }
                var contents = stringBuilder.ToString();
                return new IsSuccessResult<(string Context, string FileName)>()
                {
                    ReturnObject = (contents, $"{name}{timestamp}.txt")
                };
            }
            catch (Exception e)
            {
                return new IsSuccessResult<(string Context, string FileName)>(e.ToString());
            }
        }

        private string GetSqlContents(object obj)
        {
            var stringBuilder = new StringBuilder();
            if (obj == null)
            {
                return stringBuilder.ToString();
            }

            stringBuilder.Append($@"INSERT INTO `{obj.GetType().Name}`(");
            var propertyInfos = obj.GetType().GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                stringBuilder.Append($"`{propertyInfo.Name}`,");
            }

            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Append(") VALUES (");
            //依照property的屬性判斷有無'' 進DB才不會出錯;
            foreach (var propertyInfo in propertyInfos)
            {
                if (propertyInfo.GetValue(obj) == null)
                {
                    stringBuilder.Append($"NULL,");
                    continue;
                }

                if (propertyInfo.PropertyType.Name == "String")
                {
                    var value = propertyInfo.GetValue(obj)?.ToString();
                    value = value.Replace("\r", @"\r").Replace("\n", @"\n").Replace("'", @"\'");
                    stringBuilder.Append($"\'{value}\',");
                    continue;
                }
                if (propertyInfo.PropertyType.Name == "DateTime")
                {
                    stringBuilder.Append($"\'{propertyInfo.GetValue(obj)}\',");
                    continue;
                }
                stringBuilder.Append($"{propertyInfo.GetValue(obj)},");
            }
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Append(");");
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }

    }
}