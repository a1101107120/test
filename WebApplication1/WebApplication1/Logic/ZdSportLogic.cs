﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Library.DbContext;
using Library.DbContextHelper;
using Library.ZdSportModel;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Logic
{
    public interface IZdSportLogic
    {
        void AddDataToTable(List<LeagueType> leagueTypeList, List<OddInfo> oddInfoList,
            List<MoreOddInfo> moreOddInfoList, List<GameInfo> gameInfoList);
    }

    public class ZdSportLogic : IZdSportLogic
    {
        private readonly ZdSportsContext _zdSportsContext;
        private readonly IZdSportsContextHelper _zdSportsContextHelper;

        public ZdSportLogic(ZdSportsContext zdSportsContext, IZdSportsContextHelper zdSportsContextHelper)
        {
            _zdSportsContext = zdSportsContext;
            _zdSportsContextHelper = zdSportsContextHelper;
        }

        public void AddDataToTable(List<LeagueType> leagueTypeList, List<OddInfo> oddInfoList,
            List<MoreOddInfo> moreOddInfoList, List<GameInfo> gameInfoList)
        {
            using (var zdSportContext = new ZdSportsContext(_zdSportsContextHelper.GetContextConnectString()))
            using (var transaction = zdSportContext.Database.BeginTransaction())
            {

                try
                {
                    var tasks = new List<Task>();
                    foreach (var data in leagueTypeList)
                    {
                        var leagueType = zdSportContext.LeagueType.FirstOrDefault(r => r.Code == data.Code);
                        if (leagueType == null)
                        {
                            zdSportContext.LeagueType.Add(data);
                        }
                        else
                        {
                            leagueType.Code = data.Code;
                            leagueType.Name = data.Name;
                        }
                    }

                    foreach (var data in oddInfoList)
                    {
                        var oddInfo = zdSportContext.OddInfo.FirstOrDefault(r => r.GemeInfoId == data.GemeInfoId && r.PageTypeCode == data.PageTypeCode && r.OddTypeCode == data.OddTypeCode && r.AreaTypeCode == data.AreaTypeCode);
                        if (oddInfo == null)
                        {
                            zdSportContext.OddInfo.Add(data);
                        }
                        else
                        {
                            oddInfo.Odds = data.Odds;
                            oddInfo.OddsName = data.OddsName;
                            oddInfo.OddsPropertyName = data.OddsPropertyName;
                        }
                    }

                    foreach (var data in moreOddInfoList)
                    {
                        var moreOddInfo = zdSportContext.MoreOddInfo.FirstOrDefault(r => r.GemeInfoId == data.GemeInfoId && r.PageTypeCode == data.PageTypeCode && r.AreaTypeCode == data.AreaTypeCode);
                        if (moreOddInfo == null)
                        {
                            zdSportContext.MoreOddInfo.Add(data);
                        }
                        else
                        {
                            moreOddInfo.MoreOdds = data.MoreOdds;
                        }
                    }
                    foreach (var data in gameInfoList)
                    {
                        var gameInfo = zdSportContext.GameInfo.FirstOrDefault(r => r.AreaTypeCode == data.AreaTypeCode && r.Id == data.Id);
                        if (gameInfo == null)
                        {
                            zdSportContext.GameInfo.Add(data);
                        }
                        else
                        {
                            gameInfo.LeagueTypeCode = data.LeagueTypeCode;
                            gameInfo.ScoreTypeCode = data.ScoreTypeCode;
                            gameInfo.Time = data.Time;
                            gameInfo.PartCh = data.PartCh;
                            gameInfo.PartEn = data.PartEn;
                            gameInfo.HomeTeamScore = data.HomeTeamScore;
                            gameInfo.AwayTeamScore = data.AwayTeamScore;
                            gameInfo.HomeTeamRed = data.HomeTeamRed;
                            gameInfo.AwayTeamBingo = data.AwayTeamBingo;
                            gameInfo.Mores = data.Mores;
                            gameInfo.Date = data.Date;
                            gameInfo.HomeTeam = data.HomeTeam;
                            gameInfo.AwayTeam = data.AwayTeam;
                            gameInfo.IsProcessing = data.IsProcessing;
                            gameInfo.PageTypeCode = data.PageTypeCode;
                        }
                    }

                    zdSportContext.SaveChanges();
                    transaction.Commit();
                   
                }
                catch (Exception e)
                {
                    //transaction.Rollback();
                    throw;
                }
            }
        }

    }
}