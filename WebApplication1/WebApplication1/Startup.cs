using Library.DbContext;
using Library.DbContextHelper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApplication1.Logic;
using WebApplication1.Service;


namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private IWebHostEnvironment currentEnvironment { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            ////�[�JMQ
            //services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
            //{
            //    ConnectionFactory factory = new ConnectionFactory
            //    {
            //        HostName = "192.168.50.121",
            //        UserName = "guest",
            //        Password = "guest",
            //        VirtualHost = "/",
            //        Port = 5672,
            //    };
            //    return new DefaultRabbitMQPersistentConnection(factory);
            //});

            //services.AddSingleton<IEventBus, EventBusRabbitMq>(sp =>
            //{
            //    var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
            //    //var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
            //    //var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
            //    //var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
            //    //return new EventBusRabbitMq(rabbitMQPersistentConnection, iLifetimeScope,
            //    //    eventBusSubcriptionsManager);
            //    return new EventBusRabbitMq(rabbitMQPersistentConnection);
            //});

            // MVC Razor
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();
            // Razor Page
            services.AddRazorPages()
                .AddRazorRuntimeCompilation();

            services.AddDbContext<ZdCapitalOrderContext>(options =>
            {
                var connection = Configuration["AppSettings:ZdCapitalOrderConnectionString"];
                options.UseMySQL(connection);
            });
            services.AddDbContext<FakeOtherPayContext>(options =>
            {
                var connection = Configuration["AppSettings:FakeOtherPayConnectionString"];
                options.UseMySQL(connection);
            });
            
            services.AddDbContext<ZdSportsContext>(options =>
            {
                var connection = Configuration["AppSettings:ZdSportsContextConnectionString"];
                options.UseMySQL(connection);
            });
            services.AddTransient<IDisbursementLogic, DisbursementLogic>();
            services.AddTransient<IZdSportLogic, ZdSportLogic>();
            services.AddTransient<IZdSportsContextHelper, ZdSportsContextHelper>();
            //new BrowserService(Configuration).GetStockData().GetAwaiter();
            services.AddHttpClient<HttpClientHelper>();
            services.AddSingleton<IHttpClientHelper, HttpClientHelper>();
            //test1

            //test

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            currentEnvironment = env;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

    }

}
