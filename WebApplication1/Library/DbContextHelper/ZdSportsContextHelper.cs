﻿
using Library.DbContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Library.DbContextHelper
{
    public interface IZdSportsContextHelper
    {
        DbContextOptions<ZdSportsContext> GetContextOptions();
        string GetContextConnectString();
    }

    public class ZdSportsContextHelper : IZdSportsContextHelper
    {
        private readonly IConfiguration _configuration;
        public ZdSportsContextHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetContextConnectString()
        {
            var connectionString = _configuration["AppSettings:ZdSportsContextConnectionString"];
            return connectionString;
        }
        public DbContextOptions<ZdSportsContext> GetContextOptions()
        {
            var connectionString = _configuration["AppSettings:ZdSportsContextConnectionString"];
            return new DbContextOptionsBuilder<ZdSportsContext>()
                .UseMySQL(connectionString)
                .Options;
        }
    }
}