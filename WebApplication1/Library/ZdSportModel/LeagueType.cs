﻿namespace Library.ZdSportModel
{
    public partial class LeagueType
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
