﻿namespace Library.ZdSportModel
{
    public partial class Games
    {
        public long Id { get; set; }
        public int SportsTypeCode { get; set; }
        public int LeagueTypeCode { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public string Position { get; set; }
    }
}
