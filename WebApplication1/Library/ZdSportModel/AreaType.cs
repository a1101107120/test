﻿namespace Library.ZdSportModel
{
    public partial class AreaType
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
