﻿namespace Library.ZdSportModel
{
    public partial class MoreOddInfo
    {
        public string GemeInfoId { get; set; }
        public int PageTypeCode { get; set; }
        public int OddTypeCode { get; set; }
        public int AreaTypeCode { get; set; }
        public string MoreOdds { get; set; }
    }
}
