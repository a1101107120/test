﻿namespace Library.ZdSportModel
{
    public partial class OddType
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
