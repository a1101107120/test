﻿namespace Library.ZdSportModel
{
    public partial class OddInfo
    {
        public string GemeInfoId { get; set; }
        public int PageTypeCode { get; set; }
        public int OddTypeCode { get; set; }
        public int AreaTypeCode { get; set; }
        public string Odds { get; set; }
        public string OddsName { get; set; }
        public string OddsPropertyName { get; set; }
    }
}
