﻿namespace Library.ZdSportModel
{
    public partial class GameInfo
    {
        public string Id { get; set; }
        public long LeagueTypeCode { get; set; }
        public int ScoreTypeCode { get; set; }
        public string Time { get; set; }
        public string PartCh { get; set; }
        public string PartEn { get; set; }
        public string HomeTeamScore { get; set; }
        public string AwayTeamScore { get; set; }
        public string HomeTeamRed { get; set; }
        public string AwayTeamRed { get; set; }
        public string HomeTeamBingo { get; set; }
        public string AwayTeamBingo { get; set; }
        public string Mores { get; set; }
        public string Date { get; set; }
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public int AreaTypeCode { get; set; }
        public bool IsProcessing { get; set; }
        public int PageTypeCode { get; set; }
        public int? ScoreCode { get; set; }
        public string ScoreTypeName { get; set; }
    }
}
