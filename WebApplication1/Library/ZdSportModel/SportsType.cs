﻿namespace Library.ZdSportModel
{
    public partial class SportsType
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
