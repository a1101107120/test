﻿namespace Library.ZdSportModel
{
    public partial class ScoreType
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
