﻿namespace Library.ZdSportModel
{
    public partial class GameAccounts
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string SessionId { get; set; }
        public string Xauth { get; set; }
    }
}
