﻿namespace Library.ZdSportModel
{
    public partial class PageType
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
