﻿using Library.FakeOtherPayDbModel;
using Microsoft.EntityFrameworkCore;

namespace Library.DbContext
{
    public partial class FakeOtherPayContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public FakeOtherPayContext()
        {
        }

        public FakeOtherPayContext(DbContextOptions<FakeOtherPayContext> options)
            : base(options)
        {
        }

        public virtual DbSet<OtherPay> OtherPay { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseMySQL("server=192.168.50.146;port=3306;user=root;password=qwe123123;Character Set=utf8;database=FakeOtherPay");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OtherPay>(entity =>
            {
                entity.HasKey(e => e.OtherPayCode)
                    .HasName("PRIMARY");

                entity.ToTable("OtherPay", "fakeotherpay");

                entity.Property(e => e.OtherPayCode).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ResponseData).HasDefaultValueSql("'''NULL'''");

                entity.Property(e => e.Type).HasColumnType("int(11)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
