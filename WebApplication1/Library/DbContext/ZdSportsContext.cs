﻿using Library.ZdSportModel;
using Microsoft.EntityFrameworkCore;

namespace Library.DbContext
{
    public partial class ZdSportsContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private readonly string _connectionString;
        public ZdSportsContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ZdSportsContext(DbContextOptions<ZdSportsContext> options)
            : base(options)
        {
           
        }

        public virtual DbSet<AreaType> AreaType { get; set; }
        public virtual DbSet<GameAccounts> GameAccounts { get; set; }
        public virtual DbSet<GameInfo> GameInfo { get; set; }
        public virtual DbSet<Games> Games { get; set; }
        public virtual DbSet<LeagueType> LeagueType { get; set; }
        public virtual DbSet<MoreOddInfo> MoreOddInfo { get; set; }
        public virtual DbSet<OddInfo> OddInfo { get; set; }
        public virtual DbSet<OddType> OddType { get; set; }
        public virtual DbSet<PageType> PageType { get; set; }
        public virtual DbSet<ScoreType> ScoreType { get; set; }
        public virtual DbSet<SportsType> SportsType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AreaType>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("AreaType", "zdsports");

                entity.HasComment(@"地區類型
1:歐洲盤 2:香港盤");

                entity.Property(e => e.Code)
                    .HasColumnType("int(11)")
                    .HasComment("類型編碼");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("''''''")
                    .HasComment("類型名稱");
            });

            modelBuilder.Entity<GameAccounts>(entity =>
            {
                entity.HasKey(e => e.Account)
                    .HasName("PRIMARY");

                entity.ToTable("GameAccounts", "zdsports");

                entity.HasComment("遊戲帳號(帳號池)");

                entity.Property(e => e.Account)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("''''''")
                    .HasComment("帳號");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("''''''")
                    .HasComment("密碼");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasDefaultValueSql("''''''");

                entity.Property(e => e.Xauth)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("''''''");
            });

            modelBuilder.Entity<GameInfo>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.AreaTypeCode })
                    .HasName("PRIMARY");

                entity.ToTable("GameInfo", "zdsports");

                entity.HasComment("比賽資訊");

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .HasComment("比賽盤口ID");

                entity.Property(e => e.AreaTypeCode)
                    .HasColumnType("int(3)")
                    .HasComment("地區盤口");

                entity.Property(e => e.AwayTeam)
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("客隊名稱");

                entity.Property(e => e.AwayTeamBingo)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("客隊賓果");

                entity.Property(e => e.AwayTeamRed)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("客隊紅卡");

                entity.Property(e => e.AwayTeamScore)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("客隊比數");

                entity.Property(e => e.Date)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("比賽日期");

                entity.Property(e => e.HomeTeam)
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("主隊名稱");

                entity.Property(e => e.HomeTeamBingo)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("主隊賓果");

                entity.Property(e => e.HomeTeamRed)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("主隊紅卡");

                entity.Property(e => e.HomeTeamScore)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("主隊比數");

                entity.Property(e => e.LeagueTypeCode)
                    .HasColumnType("bigint(20)")
                    .HasComment("聯賽ID");

                entity.Property(e => e.Mores)
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("更多玩法");

                entity.Property(e => e.PageTypeCode).HasColumnType("int(3)");

                entity.Property(e => e.PartCh)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("當前局");

                entity.Property(e => e.PartEn)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("當前局(英文)");

                entity.Property(e => e.ScoreCode)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ScoreTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("計分類型(0:一般 GemeInfoId == GameId)");

                entity.Property(e => e.ScoreTypeName)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Time)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("比賽時間");
            });

            modelBuilder.Entity<Games>(entity =>
            {
                entity.ToTable("Games", "zdsports");

                entity.HasComment("比賽場次");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20)")
                    .HasComment("場次ID");

                entity.Property(e => e.AwayTeam)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasDefaultValueSql("''''''")
                    .HasComment("客隊名稱");

                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("''''''")
                    .HasComment("比賽日期");

                entity.Property(e => e.HomeTeam)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasDefaultValueSql("''''''")
                    .HasComment("主隊名稱");

                entity.Property(e => e.LeagueTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("聯賽類型");

                entity.Property(e => e.Position)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("場地(N:中立場)");

                entity.Property(e => e.SportsTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("運動類型");

                entity.Property(e => e.Time)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("''''''")
                    .HasComment("比賽開始時間");
            });

            modelBuilder.Entity<LeagueType>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("LeagueType", "zdsports");

                entity.HasComment("聯賽類型");

                entity.Property(e => e.Code)
                    .HasColumnType("int(11)")
                    .HasComment("類型編碼");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("''''''")
                    .HasComment("類型名稱");
            });

            modelBuilder.Entity<MoreOddInfo>(entity =>
            {
                entity.HasKey(e => new { e.GemeInfoId, e.PageTypeCode, e.AreaTypeCode })
                    .HasName("PRIMARY");

                entity.ToTable("MoreOddInfo", "zdsports");

                entity.HasComment("更多賠率資訊");

                entity.Property(e => e.GemeInfoId)
                    .HasMaxLength(20)
                    .HasComment("比賽盤口ID");

                entity.Property(e => e.PageTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("盤口類型");

                entity.Property(e => e.AreaTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("地區類型");

                entity.Property(e => e.MoreOdds)
                    .IsRequired()
                    .HasColumnType("longtext")
                    .HasDefaultValueSql("''''''")
                    .HasComment("更多賠率");

                entity.Property(e => e.OddTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("賠率類型");
            });

            modelBuilder.Entity<OddInfo>(entity =>
            {
                entity.HasKey(e => new { e.GemeInfoId, e.AreaTypeCode, e.PageTypeCode, e.OddTypeCode })
                    .HasName("PRIMARY");

                entity.ToTable("OddInfo", "zdsports");

                entity.HasComment("賠率資訊");

                entity.Property(e => e.GemeInfoId)
                    .HasMaxLength(20)
                    .HasComment("比賽盤口ID");

                entity.Property(e => e.AreaTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("地區類型");

                entity.Property(e => e.PageTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("盤口類型");

                entity.Property(e => e.OddTypeCode)
                    .HasColumnType("int(11)")
                    .HasComment("賠率類型");

                entity.Property(e => e.Odds)
                    .IsRequired()
                    .HasColumnType("longtext")
                    .HasDefaultValueSql("''''''")
                    .HasComment("賠率");

                entity.Property(e => e.OddsName)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("賠率玩法");

                entity.Property(e => e.OddsPropertyName)
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<OddType>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("OddType", "zdsports");

                entity.HasComment(@"賠率類型
1:让球(ah) 2:让球-上半场(ah1st) 3:进球:大 / 小(ou) 4:进球:大 / 小-上半场(ou1st) 5:独赢(1x2) 6:独赢-上半场(1x21st) 7:进球:单 / 双(oe) 8:进球:单 / 双-上半场(oe1st) 9:波胆(cs) 10:波胆-上半场(cs1st)
11:总进球数(tg) 12:总进球数-上半场(tg1st) 13:半场 / 全场(hf) 14:最先进球(tts1st) 15:最后进球(ttslast) 16:最先进球球员(sco1st) 17:最后进球球员(scolast) 18:进球球员(scoant)");

                entity.Property(e => e.Code)
                    .HasColumnType("int(11)")
                    .HasComment("類型編碼");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasDefaultValueSql("''''''")
                    .HasComment("類型名稱");
            });

            modelBuilder.Entity<PageType>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("PageType", "zdsports");

                entity.HasComment("盤口類型(\"pt\" 1:今日 2:早盤 3:連串過關 4:點選滾球中項目 5:我的收藏)");

                entity.Property(e => e.Code)
                    .HasColumnType("int(11)")
                    .HasComment("類型編碼");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("''''''")
                    .HasComment("類型名稱");
            });

            modelBuilder.Entity<ScoreType>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("ScoreType", "zdsports");

                entity.HasComment(@"計分類型
0:一般 1:角球 2:罚牌数 3:加时赛 5:点球大战 12:球队进球数 13:球队进球数");

                entity.Property(e => e.Code)
                    .HasColumnType("int(11)")
                    .HasComment("類型編碼");

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("類型名稱");
            });

            modelBuilder.Entity<SportsType>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("SportsType", "zdsports");

                entity.HasComment(@"運動類型
1:足球 2:籃球 3:網球 5:高尔夫球 6:彩票 7:美式足球 10:金融投注 13:排球 15:特别项目 16:手球 20:乒乓球 21:斯诺克/台球 23:电子竞技 26:冰球");

                entity.Property(e => e.Code)
                    .HasColumnType("int(11)")
                    .HasComment("類型編碼");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasDefaultValueSql("''''''")
                    .HasComment("類型名稱");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
