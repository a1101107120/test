﻿namespace Library.DbModel
{
    public partial class BankType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Sequence { get; set; }
        public byte IsDelete { get; set; }
    }
}
