﻿namespace Library.DbModel
{
    public partial class MerchantPayInfo
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherPayImpId { get; set; }
        public string OtherPayMarkCode { get; set; }
        public string BankcardId { get; set; }
        public string ShowName { get; set; }
        public byte Mode { get; set; }
        public string RechargeModeId { get; set; }
        public string Domain { get; set; }
        public int MinLoad { get; set; }
        public int MaxLoad { get; set; }
        public decimal Rate { get; set; }
        public byte? IsShowCashier { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
        public decimal? ExchangeRate { get; set; }
    }
}
