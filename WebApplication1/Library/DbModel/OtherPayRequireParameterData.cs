﻿namespace Library.DbModel
{
    public partial class OtherPayRequireParameterData
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string MerchantPayId { get; set; }
        public string OtherPayRequireParameterId { get; set; }
        public string ParameterValue { get; set; }
        public byte IsDelete { get; set; }
    }
}
