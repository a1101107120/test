﻿using System;

namespace Library.DbModel
{
    public partial class MerchantDisbursementOrder
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string MerchantId { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherDisbursementImpId { get; set; }
        public string MerchantPushDataId { get; set; }
        public string MerchantDisbursementInfoId { get; set; }
        public string BankTypeId { get; set; }
        public string MerchantOrderNo { get; set; }
        public string OtherOrderNo { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string AccountOpenAddress { get; set; }
        public decimal Amount { get; set; }
        public decimal FeeAmount { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string ErrorMessage { get; set; }
        public long ClientIp { get; set; }
        public string LoginName { get; set; }
        public string LockUserId { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
