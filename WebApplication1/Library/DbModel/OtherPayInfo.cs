﻿namespace Library.DbModel
{
    public partial class OtherPayInfo
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte PayType { get; set; }
        public byte IsDelete { get; set; }
    }
}
