﻿using System;

namespace Library.DbModel
{
    public partial class MerchantBankcardIncome
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string TransferOrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string BankOrderNo { get; set; }
        public string BusinessChannel { get; set; }
        public string BankPostscript { get; set; }
        public string BankPlayName { get; set; }
        public string BankcardNo { get; set; }
        public string BankcardAccount { get; set; }
        public string IncomeSource { get; set; }
        public decimal Amount { get; set; }
        public string DataSource { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
