﻿namespace Library.DbModel
{
    public partial class MerchantDisbursementInfo
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherDisbursementImpId { get; set; }
        public string OtherPayMarkCode { get; set; }
        public string ShowName { get; set; }
        public decimal AvailableAmount { get; set; }
        public int MinLoad { get; set; }
        public int MaxLoad { get; set; }
        public decimal Rate { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
        public decimal? ExchangeRate { get; set; }
    }
}
