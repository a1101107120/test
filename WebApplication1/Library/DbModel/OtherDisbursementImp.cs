﻿namespace Library.DbModel
{
    public partial class OtherDisbursementImp
    {
        public string Id { get; set; }
        public string OtherPayInfoId { get; set; }
        public string Name { get; set; }
        public string DescriptionContent { get; set; }
        public string SignName { get; set; }
        public string Encoding { get; set; }
        public string ParameterType { get; set; }
        public string SignFixedParameter { get; set; }
        public string QuerySignFixedParameter { get; set; }
        public string QueryAmountSignFixedParameter { get; set; }
        public string EncryptionHelperId { get; set; }
        public string SignHelperId { get; set; }
        public string OrderNoHelperId { get; set; }
        public string SignEndHelperId { get; set; }
        public string CreateOrderHelperId { get; set; }
        public string OrderCallbackHelperId { get; set; }
        public string QueryAmountHelperId { get; set; }
        public string RequestReturnJavascript { get; set; }
        public string CallbackJavascript { get; set; }
        public string QueryAmountJavascript { get; set; }
        public int MinLoad { get; set; }
        public int MaxLoad { get; set; }
        public int Sequence { get; set; }
        public byte RequestMode { get; set; }
        public byte? QueryMode { get; set; }
        public bool QueryAmount { get; set; }
        public bool Status { get; set; }
        public string CreateOrderAddress { get; set; }
        public string QueryOrderAddress { get; set; }
        public string QueryAmountAddress { get; set; }
        public byte IsDelete { get; set; }
        public byte DigiCcy { get; set; }
        public string CurrencyTypeId { get; set; }
    }
}
