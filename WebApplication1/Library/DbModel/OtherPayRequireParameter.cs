﻿namespace Library.DbModel
{
    public partial class OtherPayRequireParameter
    {
        public string Id { get; set; }
        public string ParameterName { get; set; }
        public string ShowName { get; set; }
        public string OtherPayInfoId { get; set; }
        public byte PayType { get; set; }
        public int Sequence { get; set; }
        public byte IsDelete { get; set; }
    }
}
