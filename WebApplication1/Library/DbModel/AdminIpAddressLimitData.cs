﻿namespace Library.DbModel
{
    public partial class AdminIpAddressLimitData
    {
        public string Id { get; set; }
        public string AdminId { get; set; }
        public string DescriptionContent { get; set; }
        public long IpAddress { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
    }
}
