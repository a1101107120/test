﻿namespace Library.DbModel
{
    public partial class SysConfig
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string ItemKey { get; set; }
        public string ItemValue { get; set; }
        public string DescriptionContent { get; set; }
        public int Sequence { get; set; }
        public byte IsDelete { get; set; }
    }
}
