﻿namespace Library.DbModel
{
    public partial class MerchantPushData
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string PlatformName { get; set; }
        public string PlatformMark { get; set; }
        public string PushAddress { get; set; }
        public string DisbursementOrderPushAddress { get; set; }
        public string ChatServiceAddress { get; set; }
        public int RandomMinNum { get; set; }
        public int RandomMaxNum { get; set; }
        public byte IsDelete { get; set; }
    }
}
