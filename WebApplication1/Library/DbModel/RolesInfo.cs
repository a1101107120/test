﻿namespace Library.DbModel
{
    public partial class RolesInfo
    {
        public int Id { get; set; }
        public byte SystemType { get; set; }
        public string Name { get; set; }
        public string MerchantId { get; set; }
        public string Remarks { get; set; }
        public bool IsDelete { get; set; }
    }
}
