﻿using System;

namespace Library.DbModel
{
    public partial class MerchantDisbursementOrderDigiCcy
    {
        public string Id { get; set; }
        public string MerchantPushDataId { get; set; }
        public string OtherPayInfoId { get; set; }
        public string MerchantDisbursementInfoId { get; set; }
        public string OtherDisbursementImpId { get; set; }
        public string MerchantId { get; set; }
        public string MerchantOrderNo { get; set; }
        public string LoginName { get; set; }
        public string Currency { get; set; }
        public string WalletAddress { get; set; }
        public string Protocol { get; set; }
        public decimal? Amount { get; set; }
        public decimal? RealAmount { get; set; }
        public decimal? DigiCcyquantity { get; set; }
        public decimal? RealDigiCcyquantity { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal FeeAmount { get; set; }
        public byte Status { get; set; }
        public string ErrorMessage { get; set; }
        public byte IsDelete { get; set; }
        public string LockUserId { get; set; }
        public DateTime Date { get; set; }
        public string OtherOrderNo { get; set; }
        public long ClientIp { get; set; }
    }
}
