﻿namespace Library.DbModel
{
    public partial class OtherPayHelperImp
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DescriptionContent { get; set; }
        public string Assembly { get; set; }
        public string Namespace { get; set; }
        public string ImpClass { get; set; }
        public byte Type { get; set; }
        public byte PayType { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
