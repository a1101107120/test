﻿namespace Library.DbModel
{
    public partial class OtherPayBankTypeMaps
    {
        public string Id { get; set; }
        public string BankTypeId { get; set; }
        public string OtherPayImpId { get; set; }
        public string OtherPayBankTypeCode { get; set; }
        public string OtherPayBankTypeName { get; set; }
        public byte PayType { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
