﻿using System;

namespace Library.DbModel
{
    public partial class MerchantRevenueData
    {
        public int Id { get; set; }
        public string MerchantId { get; set; }
        public string MerchantPushDataId { get; set; }
        public DateTime Date { get; set; }
        public bool Mode { get; set; }
        public string PayInfoName { get; set; }
        public string GewayName { get; set; }
        public string BankType { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public int TotalCount { get; set; }
        public int PayCount { get; set; }
        public decimal RealAmount { get; set; }
        public byte Status { get; set; }
    }
}
