﻿namespace Library.DbModel
{
    public partial class OtherPayCurrencyTypeMaps
    {
        public string Id { get; set; }
        public string CurrencyTypeId { get; set; }
        public string OtherPayImpId { get; set; }
        public string OtherPayCurrencyTypeCode { get; set; }
        public string OtherPayCurrencyTypeName { get; set; }
        public byte PayType { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
