﻿namespace Library.DbModel
{
    public partial class MerchantDisbursementOrderDataDetail
    {
        public string Id { get; set; }
        public string OrderId { get; set; }
        public string RequestData { get; set; }
        public string ResponseData { get; set; }
        public string CallBackRequestData { get; set; }
        public string CallBackResponseData { get; set; }
        public string PushRequestData { get; set; }
        public string PushResponseData { get; set; }
        public string QueryOrderResponse { get; set; }
        public byte PushStatus { get; set; }
        public int PushCount { get; set; }
        public byte IsDelete { get; set; }
    }
}
