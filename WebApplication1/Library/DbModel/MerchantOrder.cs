﻿using System;

namespace Library.DbModel
{
    public partial class MerchantOrder
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string MerchantId { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherPayImpId { get; set; }
        public string MerchantPayInfoId { get; set; }
        public string BankcardId { get; set; }
        public string RechargeModeId { get; set; }
        public string MerchantPushDataId { get; set; }
        public string MerchantOrderNo { get; set; }
        public string OtherPayOrderNo { get; set; }
        public string LoginName { get; set; }
        public decimal Amount { get; set; }
        public decimal RealAmount { get; set; }
        public long ClientIp { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
