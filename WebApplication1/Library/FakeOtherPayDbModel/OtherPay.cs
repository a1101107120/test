﻿using System;
using System.Collections.Generic;

namespace Library.FakeOtherPayDbModel
{
    public partial class OtherPay
    {
        public string OtherPayCode { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public string ResponseData { get; set; }
    }
}
