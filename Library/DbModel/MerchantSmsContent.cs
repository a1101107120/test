﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantSmsContent
    {
        public string Id { get; set; }
        public DateTime? Date { get; set; }
        public string MerchantId { get; set; }
        public string BankcardId { get; set; }
        public string BankcardNo { get; set; }
        public string BankcardAccount { get; set; }
        public string BankcardPhone { get; set; }
        public string TxtContent { get; set; }
        public string Sender { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
