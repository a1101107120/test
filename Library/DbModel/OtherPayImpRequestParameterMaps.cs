﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class OtherPayImpRequestParameterMaps
    {
        public string Id { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherPayImpId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterMapTo { get; set; }
        public byte ScopeType { get; set; }
        public byte PayType { get; set; }
        public string Value { get; set; }
        public int Sequence { get; set; }
        public byte IsDelete { get; set; }
    }
}
