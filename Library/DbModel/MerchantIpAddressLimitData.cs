﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantIpAddressLimitData
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string DescriptionContent { get; set; }
        public long IpAddress { get; set; }
        public bool? IsOpenApiLimit { get; set; }
        public bool? IsOpenWebSiteLimit { get; set; }
        public bool IsHide { get; set; }
        public bool Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
