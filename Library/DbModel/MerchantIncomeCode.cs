﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantIncomeCode
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public int? CodeType { get; set; }
        public string Name { get; set; }
        public string QrUrl { get; set; }
        public int Sequence { get; set; }
        public byte? Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
