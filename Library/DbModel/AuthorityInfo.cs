﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class AuthorityInfo
    {
        public int Id { get; set; }
        public byte SystemType { get; set; }
        public string Name { get; set; }
        public int ModuleId { get; set; }
        public string AuthMark { get; set; }
        public bool IsDelete { get; set; }
    }
}
