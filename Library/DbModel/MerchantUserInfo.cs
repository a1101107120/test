﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantUserInfo
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }
        public byte Status { get; set; }
        public long LastLoginIpAddress { get; set; }
        public byte IsDelete { get; set; }
    }
}
