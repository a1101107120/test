﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantBankcard
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string BankTypeId { get; set; }
        public string BankcardNo { get; set; }
        public string BankcardShowNo { get; set; }
        public string BankcardAccount { get; set; }
        public string BankcardOpenAddress { get; set; }
        public string BankcardPhone { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
