﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantPayInfoBankcard
    {
        public string Id { get; set; }
        public string MerchantId { get; set; }
        public string MerchantPayInfoId { get; set; }
        public string BankcardId { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
