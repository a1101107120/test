﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class OtherPayImp
    {
        public string Id { get; set; }
        public string OtherPayInfoId { get; set; }
        public string RechargeModeId { get; set; }
        public string Name { get; set; }
        public string DescriptionContent { get; set; }
        public string SignName { get; set; }
        public string InterfaceVersion { get; set; }
        public string Encoding { get; set; }
        public string ParameterType { get; set; }
        public string SignFixedParameter { get; set; }
        public string QuerySignFixedParameter { get; set; }
        public string EncryptionHelperId { get; set; }
        public string SignHelperId { get; set; }
        public string RequestReturnHelperId { get; set; }
        public string OrderNoHelperId { get; set; }
        public string SignEndHelperId { get; set; }
        public string CreateOrderHelperId { get; set; }
        public string OrderCallbackHelperId { get; set; }
        public string CallbackJavascript { get; set; }
        public string RequestReturnJavascript { get; set; }
        public string PayType { get; set; }
        public int MinLoad { get; set; }
        public int MaxLoad { get; set; }
        public byte? LimitAmountType { get; set; }
        public string AmountFixedParameter { get; set; }
        public int Sequence { get; set; }
        public byte RequestMode { get; set; }
        public byte? QueryMode { get; set; }
        public byte? PollingType { get; set; }
        public bool? IsRandomAmount { get; set; }
        public bool? IsOpenPostscript { get; set; }
        public bool IsShowCallback { get; set; }
        public bool? IsNotifyCustomerService { get; set; }
        public string ShowCallbackController { get; set; }
        public byte? ApplicationScope { get; set; }
        public byte? PostscriptLength { get; set; }
        public byte? MatchingTime { get; set; }
        public bool? Mode { get; set; }
        public bool Status { get; set; }
        public bool? RequirePayName { get; set; }
        public string CreateOrderAddress { get; set; }
        public string QueryOrderAddress { get; set; }
        public string QueryAmountAddress { get; set; }
        public byte IsDelete { get; set; }
        public string CurrencyTypeId { get; set; }
    }
}
