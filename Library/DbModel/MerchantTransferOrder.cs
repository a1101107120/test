﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MerchantTransferOrder
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string MerchantId { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherPayImpId { get; set; }
        public string MerchantPushDataId { get; set; }
        public string MerchantPayInfoId { get; set; }
        public string IncomeCodeId { get; set; }
        public string BankcardId { get; set; }
        public string BankcardNo { get; set; }
        public string BankcardAccount { get; set; }
        public string RechargeModeId { get; set; }
        public string MerchantOrderNo { get; set; }
        public string OtherOrderNo { get; set; }
        public string LoginName { get; set; }
        public decimal Amount { get; set; }
        public string PayName { get; set; }
        public string Postscript { get; set; }
        public decimal RealAmount { get; set; }
        public string BankPayName { get; set; }
        public string BankPostscript { get; set; }
        public long ClientIp { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
