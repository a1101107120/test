﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class AdministrativeArea
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public byte Type { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
        public int? Sequence { get; set; }
        public string Memo { get; set; }
        public int? Status { get; set; }
    }
}
