﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class RechargeMode
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public byte Type { get; set; }
        public int Sequence { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
