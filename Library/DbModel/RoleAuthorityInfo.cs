﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class RoleAuthorityInfo
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int ModuleId { get; set; }
        public string ControllerUrl { get; set; }
        public int AuthorityId { get; set; }
        public string AuthMark { get; set; }
        public bool IsDelete { get; set; }
    }
}
