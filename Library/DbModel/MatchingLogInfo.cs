﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class MatchingLogInfo
    {
        public int Id { get; set; }
        public string OtherPayInfoId { get; set; }
        public string OtherPayImpId { get; set; }
        public string MerchantPayInfoId { get; set; }
        public string MerchantId { get; set; }
        public string OrderId { get; set; }
        public byte MatchMode { get; set; }
        public string Operator { get; set; }
        public byte Mode { get; set; }
        public string MerchantOrderNo { get; set; }
        public string MatchData { get; set; }
        public string Remarks { get; set; }
        public bool IsDelete { get; set; }
    }
}
