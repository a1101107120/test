﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class Merchant
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Md5SecretKey { get; set; }
        public string RsaPublicKey { get; set; }
        public string GoogleSecretKey { get; set; }
        public byte EncryptionType { get; set; }
        public int Sequence { get; set; }
        public bool IsOpenApiLimit { get; set; }
        public bool IsOpenWebSiteLimit { get; set; }
        public byte Status { get; set; }
        public byte IsDelete { get; set; }
    }
}
