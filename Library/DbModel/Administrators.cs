﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class Administrators
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public byte Status { get; set; }
        public long LastLoginIpAddress { get; set; }
        public byte IsDelete { get; set; }
    }
}
