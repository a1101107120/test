﻿using System;
using System.Collections.Generic;

namespace Library.DbModel
{
    public partial class ModulesInfo
    {
        public int Id { get; set; }
        public int Sequence { get; set; }
        public byte SystemType { get; set; }
        public string Name { get; set; }
        public string ControllerUrl { get; set; }
        public string ViewUrl { get; set; }
        public byte Type { get; set; }
        public int ParentsId { get; set; }
        public string IconClass { get; set; }
        public bool IsDelete { get; set; }
    }
}
