﻿using Library.DbModel;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql;

namespace Library.DbContext
{
    public partial class ZdCapitalOrderContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public ZdCapitalOrderContext()
        {
        }

        public ZdCapitalOrderContext(DbContextOptions<ZdCapitalOrderContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdministrativeArea> AdministrativeArea { get; set; }
        public virtual DbSet<Administrators> Administrators { get; set; }
        public virtual DbSet<AuthorityInfo> AuthorityInfo { get; set; }
        public virtual DbSet<BankType> BankType { get; set; }
        public virtual DbSet<CurrencyType> CurrencyType { get; set; }
        public virtual DbSet<MatchingLogInfo> MatchingLogInfo { get; set; }
        public virtual DbSet<Merchant> Merchant { get; set; }
        public virtual DbSet<MerchantBankcard> MerchantBankcard { get; set; }
        public virtual DbSet<MerchantBankcardIncome> MerchantBankcardIncome { get; set; }
        public virtual DbSet<MerchantDisbursementInfo> MerchantDisbursementInfo { get; set; }
        public virtual DbSet<MerchantDisbursementOrder> MerchantDisbursementOrder { get; set; }
        public virtual DbSet<MerchantDisbursementOrderDataDetail> MerchantDisbursementOrderDataDetail { get; set; }
        public virtual DbSet<MerchantDisbursementOrderDigiCcy> MerchantDisbursementOrderDigiCcy { get; set; }
        public virtual DbSet<MerchantIncomeCode> MerchantIncomeCode { get; set; }
        public virtual DbSet<MerchantIpAddressLimitData> MerchantIpAddressLimitData { get; set; }
        public virtual DbSet<MerchantOrder> MerchantOrder { get; set; }
        public virtual DbSet<MerchantOrderDataDetail> MerchantOrderDataDetail { get; set; }
        public virtual DbSet<MerchantOrderDigiCcy> MerchantOrderDigiCcy { get; set; }
        public virtual DbSet<MerchantPayInfo> MerchantPayInfo { get; set; }
        public virtual DbSet<MerchantPayInfoBankcard> MerchantPayInfoBankcard { get; set; }
        public virtual DbSet<MerchantPayInfoIncomeCode> MerchantPayInfoIncomeCode { get; set; }
        public virtual DbSet<MerchantPushData> MerchantPushData { get; set; }
        public virtual DbSet<MerchantRevenueData> MerchantRevenueData { get; set; }
        public virtual DbSet<MerchantSmsContent> MerchantSmsContent { get; set; }
        public virtual DbSet<MerchantTransferOrder> MerchantTransferOrder { get; set; }
        public virtual DbSet<MerchantUserInfo> MerchantUserInfo { get; set; }
        public virtual DbSet<ModulesInfo> ModulesInfo { get; set; }
        public virtual DbSet<OtherDisbursementImp> OtherDisbursementImp { get; set; }
        public virtual DbSet<OtherPayBankTypeMaps> OtherPayBankTypeMaps { get; set; }
        public virtual DbSet<OtherPayCurrencyTypeMaps> OtherPayCurrencyTypeMaps { get; set; }
        public virtual DbSet<OtherPayHelperImp> OtherPayHelperImp { get; set; }
        public virtual DbSet<OtherPayImp> OtherPayImp { get; set; }
        public virtual DbSet<OtherPayImpRequestParameterMaps> OtherPayImpRequestParameterMaps { get; set; }
        public virtual DbSet<OtherPayInfo> OtherPayInfo { get; set; }
        public virtual DbSet<OtherPayRequireParameter> OtherPayRequireParameter { get; set; }
        public virtual DbSet<OtherPayRequireParameterData> OtherPayRequireParameterData { get; set; }
        public virtual DbSet<RechargeMode> RechargeMode { get; set; }
        public virtual DbSet<RoleAuthorityInfo> RoleAuthorityInfo { get; set; }
        public virtual DbSet<RolesInfo> RolesInfo { get; set; }
        public virtual DbSet<SysConfig> SysConfig { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseMySQL("server=192.168.50.146;port=4040;user=root;password=qwe123123;Character Set=utf8;database=ZdCapitalOrder");
        //        //optionsBuilder.UseMySQL("server=localhost;port=4040;user=root;password=a123456;Character Set=utf8;database=ZdCapitalOrder");
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdministrativeArea>(entity =>
            {
                entity.ToTable("AdministrativeArea", "zdcapitalorder");

                entity.HasComment("行政区域信息");

                entity.HasIndex(e => e.Code)
                    .HasName("Index_1");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasComment("自增列");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasComment("省份代码");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("省份名称");

                entity.Property(e => e.Lat)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("纬度");

                entity.Property(e => e.Lng)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("经度");

                entity.Property(e => e.Memo)
                    .HasMaxLength(250)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("备注");

                entity.Property(e => e.ParentId)
                    .HasColumnType("int(11)")
                    .HasComment("父级Id");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("排序");

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("简称");

                entity.Property(e => e.Status)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("状态");

                entity.Property(e => e.Type)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'")
                    .HasComment("类型1:省2:市3:县/区4:街道");
            });

            modelBuilder.Entity<Administrators>(entity =>
            {
                entity.ToTable("Administrators", "zdcapitalorder");

                entity.HasComment("后台管理员");

                entity.HasIndex(e => e.UserName)
                    .HasName("UserName_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LastLoginIpAddress)
                    .HasColumnType("bigint(20)")
                    .HasComment("上次登录IP 使用inet_ntoa算法得出的IP");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("密码");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用 1:启用");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("用户名");
            });

            modelBuilder.Entity<AuthorityInfo>(entity =>
            {
                entity.ToTable("AuthorityInfo", "zdcapitalorder");

                entity.HasComment("权限信息表");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AuthMark)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("权限标识(Action地址)");

                entity.Property(e => e.IsDelete).HasComment("是否删除");

                entity.Property(e => e.ModuleId)
                    .HasColumnType("int(11)")
                    .HasComment("模块Id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("权限名称");

                entity.Property(e => e.SystemType)
                    .HasColumnType("tinyint(4)")
                    .HasComment("系统类型  0-管理端 1-商户端");
            });

            modelBuilder.Entity<BankType>(entity =>
            {
                entity.ToTable("BankType", "zdcapitalorder");

                entity.HasComment("系统银行类型");

                entity.HasIndex(e => e.Code)
                    .HasName("Code_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasComment("银行编号");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("银行名称");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");
            });

            modelBuilder.Entity<CurrencyType>(entity =>
            {
                entity.ToTable("CurrencyType", "zdcapitalorder");

                entity.HasComment("系统数字货币类型");

                entity.HasIndex(e => e.Code)
                    .HasName("Code_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasComment("数字货币编号");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("数字货币名称");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");
            });

            modelBuilder.Entity<MatchingLogInfo>(entity =>
            {
                entity.ToTable("MatchingLogInfo", "zdcapitalorder");

                entity.HasIndex(e => new { e.MerchantId, e.MerchantOrderNo })
                    .HasName("I_MerchantOrderNo");

                entity.HasIndex(e => new { e.MerchantId, e.OrderId })
                    .HasName("I_OrderId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.MatchData)
                    .IsRequired()
                    .HasComment("匹配请求数据");

                entity.Property(e => e.MatchMode)
                    .HasColumnType("tinyint(4)")
                    .HasComment("匹配模式   0-自动  1-手动");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户订单号");

                entity.Property(e => e.MerchantPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付通道配置ID");

                entity.Property(e => e.Mode)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'")
                    .HasComment("0:线上支付1:线下支付");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasComment("操作人");

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单Id");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付通道ID");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付类型ID");

                entity.Property(e => e.Remarks)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("''''''")
                    .HasComment("备注信息");
            });

            modelBuilder.Entity<Merchant>(entity =>
            {
                entity.ToTable("Merchant", "zdcapitalorder");

                entity.HasComment("系统商户");

                entity.HasIndex(e => e.Code)
                    .HasName("Code_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasComment("商户编码,用于订单编号的产生");

                entity.Property(e => e.EncryptionType)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:MD5验签2:RSA验签");

                entity.Property(e => e.GoogleSecretKey)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("Google验证器安全码");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.IsOpenApiLimit).HasComment("API访问限制(含推送)  0关闭  1开启");

                entity.Property(e => e.IsOpenWebSiteLimit).HasComment("网站允许访问限制   0关闭  1开启");

                entity.Property(e => e.Md5SecretKey)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("MD5秘钥");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("商户名称");

                entity.Property(e => e.RsaPublicKey)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasComment("RSA公钥");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<MerchantBankcard>(entity =>
            {
                entity.ToTable("MerchantBankcard", "zdcapitalorder");

                entity.HasComment("商户银行卡");

                entity.HasIndex(e => new { e.MerchantId, e.BankcardNo })
                    .HasName("MerchantBankcard")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.BankTypeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("银行类型");

                entity.Property(e => e.BankcardAccount)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("开户人姓名");

                entity.Property(e => e.BankcardNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("银行卡号");

                entity.Property(e => e.BankcardOpenAddress)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("开户行地址");

                entity.Property(e => e.BankcardPhone)
                    .IsRequired()
                    .HasMaxLength(13)
                    .HasComment("开户手机");

                entity.Property(e => e.BankcardShowNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用 1:启用");
            });

            modelBuilder.Entity<MerchantBankcardIncome>(entity =>
            {
                entity.ToTable("MerchantBankcardIncome", "zdcapitalorder");

                entity.HasComment("商户银行入款");

                entity.HasIndex(e => e.BankOrderNo)
                    .HasName("I_OrderNo");

                entity.HasIndex(e => new { e.MerchantId, e.BankOrderNo })
                    .HasName("I_BankOrderNo");

                entity.HasIndex(e => new { e.MerchantId, e.Status })
                    .HasName("I_Status");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键ID");

                entity.Property(e => e.Amount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("收入金额");

                entity.Property(e => e.BankOrderNo)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("银行订单");

                entity.Property(e => e.BankPlayName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("付款人姓名");

                entity.Property(e => e.BankPostscript)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("银行附言");

                entity.Property(e => e.BankcardAccount)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("收款人姓名");

                entity.Property(e => e.BankcardNo)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("收款人银行卡号");

                entity.Property(e => e.BusinessChannel)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("交易渠道");

                entity.Property(e => e.DataSource)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("数据来源");

                entity.Property(e => e.IncomeSource)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("收入来源");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户ID");

                entity.Property(e => e.OrderDate)
                    .HasColumnType("date")
                    .HasComment("订单日期");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("入款状态 0:未匹配  1:自动匹配 2手动匹配 ");

                entity.Property(e => e.TransferOrderId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("转账订单ID");
            });

            modelBuilder.Entity<MerchantDisbursementInfo>(entity =>
            {
                entity.ToTable("MerchantDisbursementInfo", "zdcapitalorder");

                entity.HasComment("商户代付配置信息");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.AvailableAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("可用余额");

                entity.Property(e => e.ExchangeRate)
                    .HasColumnType("decimal(16,2)")
                    .HasDefaultValueSql("'1.00'")
                    .HasComment("數字貨幣匯率");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MaxLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最高限额");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MinLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最低限额");

                entity.Property(e => e.OtherDisbursementImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("代付实现ID");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("第三方Id");

                entity.Property(e => e.OtherPayMarkCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasComment("三方商户号");

                entity.Property(e => e.Rate)
                    .HasColumnType("decimal(3,2)")
                    .HasComment("费率");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.ShowName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("显示名称");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<MerchantDisbursementOrder>(entity =>
            {
                entity.ToTable("MerchantDisbursementOrder", "zdcapitalorder");

                entity.HasComment("商户代付订单");

                entity.HasIndex(e => e.MerchantOrderNo)
                    .HasName("I_MerchantOrderNo");

                entity.HasIndex(e => new { e.MerchantId, e.AccountNo })
                    .HasName("I_Merchant_AccountNo");

                entity.HasIndex(e => new { e.MerchantId, e.Date })
                    .HasName("I_Merchant_Date");

                entity.HasIndex(e => new { e.MerchantId, e.LoginName })
                    .HasName("I_Merchant_UserName");

                entity.HasIndex(e => new { e.MerchantId, e.MerchantOrderNo })
                    .HasName("MerchantOrderNo_Unique")
                    .IsUnique();

                entity.HasIndex(e => new { e.MerchantId, e.Status })
                    .HasName("I_Merchant_Status");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键ID");

                entity.Property(e => e.AccountName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("开户人姓名");

                entity.Property(e => e.AccountNo)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("收款卡号");

                entity.Property(e => e.AccountOpenAddress)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("开户行地址");

                entity.Property(e => e.Amount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("出款金额");

                entity.Property(e => e.BankTypeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("银行类型ID");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("市");

                entity.Property(e => e.ClientIp)
                    .HasColumnType("bigint(20)")
                    .HasComment("客户端请求IP");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasComment("订单日期");

                entity.Property(e => e.ErrorMessage)
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("用于显示错误信息");

                entity.Property(e => e.FeeAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("手续费金额");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LockUserId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("锁定人ID");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("商户方用户名");

                entity.Property(e => e.MerchantDisbursementInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户代付通道配置ID");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户ID");

                entity.Property(e => e.MerchantOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("商户订单号");

                entity.Property(e => e.MerchantPushDataId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户推送配置Id");

                entity.Property(e => e.OtherDisbursementImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("代付通道ID");

                entity.Property(e => e.OtherOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("其他订单号");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("代付类型ID");

                entity.Property(e => e.Province)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("省");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:申请成功2:出款进行中3:出款失败4:退款成功5:退款失败6:系统成功7:商户失败9:出款成功");
            });

            modelBuilder.Entity<MerchantDisbursementOrderDataDetail>(entity =>
            {
                entity.ToTable("MerchantDisbursementOrderDataDetail", "zdcapitalorder");

                entity.HasIndex(e => e.OrderId)
                    .HasName("OrderId");

                entity.HasIndex(e => e.PushStatus)
                    .HasName("PushStatus");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.CallBackRequestData)
                    .IsRequired()
                    .HasComment("订单回调参数");

                entity.Property(e => e.CallBackResponseData)
                    .IsRequired()
                    .HasComment("订单回调返回值");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("订单Id");

                entity.Property(e => e.PushCount)
                    .HasColumnType("int(11)")
                    .HasComment("推送次数");

                entity.Property(e => e.PushRequestData)
                    .IsRequired()
                    .HasComment("订单推送参数");

                entity.Property(e => e.PushResponseData)
                    .IsRequired()
                    .HasComment("订单推送返回值");

                entity.Property(e => e.PushStatus)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未推送成功1:推送成功");

                entity.Property(e => e.QueryOrderResponse)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("订单查询后返回值");

                entity.Property(e => e.RequestData)
                    .IsRequired()
                    .HasComment("订单请求参数");

                entity.Property(e => e.ResponseData)
                    .IsRequired()
                    .HasComment("订单返回值数据");
            });

            modelBuilder.Entity<MerchantDisbursementOrderDigiCcy>(entity =>
            {
                entity.ToTable("MerchantDisbursementOrderDigiCCY", "zdcapitalorder");

                entity.HasComment("数字货币代付订单");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键ID");

                entity.Property(e => e.Count)
                    .HasColumnType("int(11)")
                    .HasComment("数量");

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("币种");

                entity.Property(e => e.ErrorMessage)
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("用于显示错误信息");

                entity.Property(e => e.ExchangeRate)
                    .HasColumnType("decimal(16,2)")
                    .HasDefaultValueSql("'1.00'")
                    .HasComment("數字貨幣匯率");

                entity.Property(e => e.FeeAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("手续费金额");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LockUserId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("锁定人ID");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("商戶方用戶名");

                entity.Property(e => e.MerchantDisbursementInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户代付通道配置ID");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户ID");

                entity.Property(e => e.MerchantOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("商户订单号");

                entity.Property(e => e.MerchantPushDataId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户推送配置Id");

                entity.Property(e => e.OtherDisbursementImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("第三方代付Id");

                entity.Property(e => e.OtherOrderNo)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("代付类型ID");

                entity.Property(e => e.Protocol)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("协议");

                entity.Property(e => e.RealAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("实付金额");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:申请成功2:出款进行中3:出款失败4:退款成功5:退款失败6:系统成功7:商户失败9:出款成功");

                entity.Property(e => e.WalletAddress)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("钱包地址");
            });

            modelBuilder.Entity<MerchantIncomeCode>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MerchantIncomeCode", "zdcapitalorder");

                entity.HasComment("商户收款码");

                entity.Property(e => e.CodeType)
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("收款码类型 0:微信 1:支付宝 2:云闪付 3:银联扫码 4:QQ 5:综合码");

                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.Name)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("名称");

                entity.Property(e => e.QrUrl)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("二维码地址");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<MerchantIpAddressLimitData>(entity =>
            {
                entity.ToTable("MerchantIpAddressLimitData", "zdcapitalorder");

                entity.HasComment("商户IP地址限制表");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.DescriptionContent)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("''''''")
                    .HasComment("详细描述");

                entity.Property(e => e.IpAddress)
                    .HasColumnType("bigint(20)")
                    .HasComment("Ip地址");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.IsHide).HasComment("是否隐藏IP显示");

                entity.Property(e => e.IsOpenApiLimit)
                    .HasDefaultValueSql("'0'")
                    .HasComment("API访问限制(含推送)  0关闭  1开启");

                entity.Property(e => e.IsOpenWebSiteLimit)
                    .HasDefaultValueSql("'0'")
                    .HasComment("网站允许访问限制   0关闭  1开启");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户ID");
            });

            modelBuilder.Entity<MerchantOrder>(entity =>
            {
                entity.ToTable("MerchantOrder", "zdcapitalorder");

                entity.HasComment("商户订单");

                entity.HasIndex(e => e.MerchantOrderNo)
                    .HasName("I_MerchantOrderNo");

                entity.HasIndex(e => e.OtherPayImpId)
                    .HasName("I_OtherPayImpId");

                entity.HasIndex(e => e.OtherPayInfoId)
                    .HasName("I_OtherPayInfoId");

                entity.HasIndex(e => e.OtherPayOrderNo)
                    .HasName("I_OtherPayOrderNo");

                entity.HasIndex(e => new { e.MerchantId, e.Date })
                    .HasName("I_Merchant_Date");

                entity.HasIndex(e => new { e.MerchantId, e.LoginName })
                    .HasName("I_LoginName");

                entity.HasIndex(e => new { e.MerchantId, e.MerchantOrderNo })
                    .HasName("MerchantOrderNo_Unique")
                    .IsUnique();

                entity.HasIndex(e => new { e.MerchantId, e.Status })
                    .HasName("I_MerchantId_Status");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.Amount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("订单金额");

                entity.Property(e => e.BankcardId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment(@"银行卡Id
            银行卡收款时冗余");

                entity.Property(e => e.ClientIp)
                    .HasColumnType("bigint(20)")
                    .HasComment("客户端请求IP");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("商户方用户名");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("商户订单号");

                entity.Property(e => e.MerchantPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付通道配置ID");

                entity.Property(e => e.MerchantPushDataId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("订单推送");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付通道ID");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付类型ID");

                entity.Property(e => e.OtherPayOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("三方订单号");

                entity.Property(e => e.RealAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("实际到账金额");

                entity.Property(e => e.RechargeModeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("充值模式ID");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:申请成功2:充值成功3:回调商户失败4:系统异常");
            });

            modelBuilder.Entity<MerchantOrderDataDetail>(entity =>
            {
                entity.ToTable("MerchantOrderDataDetail", "zdcapitalorder");

                entity.HasComment("商户订单数据详情");

                entity.HasIndex(e => e.OrderId)
                    .HasName("OrderId_Unique")
                    .IsUnique();

                entity.HasIndex(e => e.PushStatus)
                    .HasName("PushStatus");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.CallBackRequestData)
                    .IsRequired()
                    .HasComment("订单回调参数");

                entity.Property(e => e.CallBackResponseData)
                    .IsRequired()
                    .HasComment("订单回调反查三方支付回傳值(success時)");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("订单Id");

                entity.Property(e => e.PushCount)
                    .HasColumnType("int(11)")
                    .HasComment("推送次数");

                entity.Property(e => e.PushRequestData)
                    .IsRequired()
                    .HasComment("订单推送客戶端参数");

                entity.Property(e => e.PushResponseData)
                    .IsRequired()
                    .HasComment("订单推送客戶端回傳值");

                entity.Property(e => e.PushStatus)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未推送成功1:推送成功");

                entity.Property(e => e.QueryOrderResponse)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("订单查询后返回值,订单回调反查三方支付回傳值(非success時)");

                entity.Property(e => e.RequestData)
                    .IsRequired()
                    .HasComment("订单请求参数");

                entity.Property(e => e.ResponseData)
                    .IsRequired()
                    .HasComment("订单返回值数据");
            });

            modelBuilder.Entity<MerchantOrderDigiCcy>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MerchantOrderDigiCCY", "zdcapitalorder");

                entity.HasComment("數字貨幣充值");

                entity.Property(e => e.Amount)
                    .HasColumnType("decimal(16,4)")
                    .HasDefaultValueSql("'0.0000'")
                    .HasComment("商戶填入收款金額");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.DigiCcyquantity)
                    .HasColumnName("DigiCCYQuantity")
                    .HasColumnType("decimal(16,4)")
                    .HasDefaultValueSql("'0.0000'")
                    .HasComment("商戶填入數字貨幣数量");

                entity.Property(e => e.ExchangeRate)
                    .HasColumnType("decimal(16,4)")
                    .HasDefaultValueSql("'1.0000'")
                    .HasComment("匯率");

                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasComment("商户方用户名");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("商户订单号");

                entity.Property(e => e.MerchantPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付通道配置ID");

                entity.Property(e => e.MerchantPushDataId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户推送配置Id");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付通道ID");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付类型ID");

                entity.Property(e => e.OtherPayOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("第三方訂單號");

                entity.Property(e => e.RealAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasDefaultValueSql("'0.0000'")
                    .HasComment("實際收款金额");

                entity.Property(e => e.RealDigiCcyquantity)
                    .HasColumnName("RealDigiCCYQuantity")
                    .HasColumnType("decimal(16,4)")
                    .HasDefaultValueSql("'0.0000'")
                    .HasComment("實際數字貨幣数量");

                entity.Property(e => e.RechargeModeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("充值模式ID");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:申请成功2:充值成功3:回调商户失败4:系统异常");
            });

            modelBuilder.Entity<MerchantPayInfo>(entity =>
            {
                entity.ToTable("MerchantPayInfo", "zdcapitalorder");

                entity.HasComment("商户支付配置信息");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.BankcardId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("银行卡Id");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("''''''")
                    .HasComment("充值域名");

                entity.Property(e => e.ExchangeRate)
                    .HasColumnType("decimal(16,2)")
                    .HasDefaultValueSql("'1.00'")
                    .HasComment("數字貨幣匯率");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.IsShowCashier)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'")
                    .HasComment("是否在收银台显示 0:不显示 1显示");

                entity.Property(e => e.MaxLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最高限额");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MinLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最低限额");

                entity.Property(e => e.Mode)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'")
                    .HasComment("0:线上支付1:线下支付");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付实现Id");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("第三方Id");

                entity.Property(e => e.OtherPayMarkCode)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasComment("三方商户号");

                entity.Property(e => e.Rate)
                    .HasColumnType("decimal(3,2)")
                    .HasComment("费率");

                entity.Property(e => e.RechargeModeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("充值模式Id");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.ShowName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("显示名称");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<MerchantPayInfoBankcard>(entity =>
            {
                entity.ToTable("MerchantPayInfoBankcard", "zdcapitalorder");

                entity.HasComment("商户支付通道银行卡绑定管理");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.BankcardId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("银行卡Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付通道ID");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<MerchantPayInfoIncomeCode>(entity =>
            {
                entity.ToTable("MerchantPayInfoIncomeCode", "zdcapitalorder");

                entity.HasComment("商户支付通道收款码绑定管理");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.IncomeCodeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("收款码ID");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付通道ID");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<MerchantPushData>(entity =>
            {
                entity.ToTable("MerchantPushData", "zdcapitalorder");

                entity.HasIndex(e => new { e.MerchantId, e.PlatformMark, e.IsDelete })
                    .HasName("I_M_Mark1")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.ChatServiceAddress)
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("客服地址");

                entity.Property(e => e.DisbursementOrderPushAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasDefaultValueSql("''''''")
                    .HasComment("提现商户平台推送地址");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.PlatformMark)
                    .IsRequired()
                    .HasMaxLength(8)
                    .HasComment("商户平台标识");

                entity.Property(e => e.PlatformName)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasComment("商户平台名称");

                entity.Property(e => e.PushAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("充值商户平台推送地址");

                entity.Property(e => e.RandomMaxNum)
                    .HasColumnType("int(11)")
                    .HasComment("随机数最大值");

                entity.Property(e => e.RandomMinNum)
                    .HasColumnType("int(11)")
                    .HasComment("随机数最小值");
            });

            modelBuilder.Entity<MerchantRevenueData>(entity =>
            {
                entity.ToTable("MerchantRevenueData", "zdcapitalorder");

                entity.HasComment("商户收入数据");

                entity.HasIndex(e => e.Date)
                    .HasName("I_Date");

                entity.HasIndex(e => e.MerchantId)
                    .HasName("I_MerchantId");

                entity.HasIndex(e => new { e.MerchantId, e.Date })
                    .HasName("I_Merchant_Date");

                entity.HasIndex(e => new { e.MerchantId, e.Date, e.MerchantPushDataId })
                    .HasName("I_Merchant_Date_Platform");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasComment("主键ID");

                entity.Property(e => e.BankType)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("银行类型");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasComment("日期");

                entity.Property(e => e.GewayName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("通道名称");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户ID");

                entity.Property(e => e.MerchantPushDataId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.Mode).HasComment("模式,0:线下支付 1:线上支付");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("收款名称");

                entity.Property(e => e.PayCount)
                    .HasColumnType("int(11)")
                    .HasComment("实付笔数");

                entity.Property(e => e.PayInfoName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("支付类型");

                entity.Property(e => e.RealAmount)
                    .HasColumnType("decimal(14,6)")
                    .HasComment("实付金额");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("数据有效性0：无效 1有效");

                entity.Property(e => e.TotalCount)
                    .HasColumnType("int(11)")
                    .HasComment("总笔数");
            });

            modelBuilder.Entity<MerchantSmsContent>(entity =>
            {
                entity.ToTable("MerchantSmsContent", "zdcapitalorder");

                entity.HasComment("短信内容");

                entity.HasIndex(e => e.MerchantId)
                    .HasName("I_MerchantId");

                entity.HasIndex(e => new { e.MerchantId, e.BankcardAccount })
                    .HasName("I_MerchatId_BankcardAccount");

                entity.HasIndex(e => new { e.MerchantId, e.BankcardId })
                    .HasName("I_MerchatId_BankcardId");

                entity.HasIndex(e => new { e.MerchantId, e.BankcardNo })
                    .HasName("I_MerchatId_BankcardNo");

                entity.HasIndex(e => new { e.MerchantId, e.Date })
                    .HasName("I_MerchantId_Date");

                entity.HasIndex(e => new { e.MerchantId, e.Status })
                    .HasName("I_MerchantId_Status");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.BankcardAccount)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("''''''")
                    .HasComment("收款户名");

                entity.Property(e => e.BankcardId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("银行卡Id\\n            银行卡收款时冗余");

                entity.Property(e => e.BankcardNo)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("收款卡号");

                entity.Property(e => e.BankcardPhone)
                    .IsRequired()
                    .HasMaxLength(11)
                    .HasComment("电话号码");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("日期");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.Sender)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("发送者");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:未读  2：已读");

                entity.Property(e => e.TxtContent)
                    .IsRequired()
                    .HasComment("短信内容");
            });

            modelBuilder.Entity<MerchantTransferOrder>(entity =>
            {
                entity.ToTable("MerchantTransferOrder", "zdcapitalorder");

                entity.HasIndex(e => e.MerchantOrderNo)
                    .HasName("I_MerchantOrderNo");

                entity.HasIndex(e => new { e.MerchantId, e.BankcardId })
                    .HasName("I_Merchant_Bankcard");

                entity.HasIndex(e => new { e.MerchantId, e.BankcardNo })
                    .HasName("I_Merchant_BankcardNo");

                entity.HasIndex(e => new { e.MerchantId, e.Date })
                    .HasName("I_Merchant_Date");

                entity.HasIndex(e => new { e.MerchantId, e.LoginName })
                    .HasName("I_Merchant_UserName");

                entity.HasIndex(e => new { e.MerchantId, e.MerchantOrderNo })
                    .HasName("MerchantOrderNo_Unique")
                    .IsUnique();

                entity.HasIndex(e => new { e.MerchantId, e.Postscript })
                    .HasName("I_Merchant_Postscript");

                entity.HasIndex(e => new { e.MerchantId, e.Status })
                    .HasName("I_Merchant_Status");

                entity.HasIndex(e => new { e.MerchantId, e.RechargeModeId, e.Status })
                    .HasName("I_Merchant_RechargeMode");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.Amount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("订单金额");

                entity.Property(e => e.BankPayName)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasDefaultValueSql("''''''")
                    .HasComment("银行收录支付人姓名");

                entity.Property(e => e.BankPostscript)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasDefaultValueSql("''''''")
                    .HasComment("银行收录附言");

                entity.Property(e => e.BankcardAccount)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("''''''")
                    .HasComment("收款户名");

                entity.Property(e => e.BankcardId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment(@"银行卡Id
            银行卡收款时冗余");

                entity.Property(e => e.BankcardNo)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("收款卡号");

                entity.Property(e => e.ClientIp)
                    .HasColumnType("bigint(20)")
                    .HasComment("客户端请求IP");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.IncomeCodeId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("商户方用户名");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("商户订单号");

                entity.Property(e => e.MerchantPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付通道配置ID");

                entity.Property(e => e.MerchantPushDataId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户推送配置Id");

                entity.Property(e => e.OtherOrderNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("其他订单号");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付通道ID");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付类型ID");

                entity.Property(e => e.PayName)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单支付人姓名");

                entity.Property(e => e.Postscript)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单附言");

                entity.Property(e => e.RealAmount)
                    .HasColumnType("decimal(16,4)")
                    .HasComment("收款金额");

                entity.Property(e => e.RechargeModeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("充值模式ID");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:申请成功2:充值成功3:回调商户失败4:系统异常");
            });

            modelBuilder.Entity<MerchantUserInfo>(entity =>
            {
                entity.ToTable("MerchantUserInfo", "zdcapitalorder");

                entity.HasComment("商户用户信息");

                entity.HasIndex(e => new { e.MerchantId, e.LoginName })
                    .HasName("MerchantUserName_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键ID");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.LastLoginIpAddress)
                    .HasColumnType("bigint(20)")
                    .HasComment("上次登录IP 使用inet_ntoa算法得出的IP");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("用户名");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户ID");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("密码");

                entity.Property(e => e.RoleId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("权限");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用 1:启用");
            });

            modelBuilder.Entity<ModulesInfo>(entity =>
            {
                entity.ToTable("ModulesInfo", "zdcapitalorder");

                entity.HasComment("模块菜单表");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ControllerUrl)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("''''''")
                    .HasComment("模块地址");

                entity.Property(e => e.IconClass)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("''''''")
                    .HasComment("图标类名");

                entity.Property(e => e.IsDelete).HasComment("是否删除");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("模块/菜单名称");

                entity.Property(e => e.ParentsId)
                    .HasColumnType("int(11)")
                    .HasComment("父级模块id");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("序列");

                entity.Property(e => e.SystemType)
                    .HasColumnType("tinyint(4)")
                    .HasComment("系统类型  0-管理端 1-商户端");

                entity.Property(e => e.Type)
                    .HasColumnType("tinyint(4)")
                    .HasComment("模块类型   0-左侧");

                entity.Property(e => e.ViewUrl)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("''''''")
                    .HasComment("视图地址");
            });

            modelBuilder.Entity<OtherDisbursementImp>(entity =>
            {
                entity.ToTable("OtherDisbursementImp", "zdcapitalorder");

                entity.HasComment("第三方代付实现");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.CallbackJavascript)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("回调时解析数据的javascript");

                entity.Property(e => e.CreateOrderAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("创建订单地址");

                entity.Property(e => e.CreateOrderHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("创建订单帮助类");

                entity.Property(e => e.DescriptionContent)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("详细描述");

                entity.Property(e => e.DigiCcy)
                    .HasColumnName("DigiCCY")
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:不是數字貨幣 1:是");

                entity.Property(e => e.Encoding)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("通道字符编码");

                entity.Property(e => e.EncryptionHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("加密帮助类");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MaxLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最大代付金额");

                entity.Property(e => e.MinLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最小代付金额");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("名称");

                entity.Property(e => e.OrderCallbackHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单回调帮助类");

                entity.Property(e => e.OrderNoHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单号帮助类");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("第三方Id");

                entity.Property(e => e.ParameterType)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("参数类型   String   Json");

                entity.Property(e => e.QueryAmount).HasComment("是否允许查询余额 0：不允许 1：允许");

                entity.Property(e => e.QueryAmountAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("查询余额地址");

                entity.Property(e => e.QueryAmountHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("查询余额帮助类");

                entity.Property(e => e.QueryAmountJavascript)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("查询余额返回值解析");

                entity.Property(e => e.QueryAmountSignFixedParameter)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("查询余额固定参数签名");

                entity.Property(e => e.QueryMode)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'")
                    .HasComment("查询模式  1:Post  2:Get");

                entity.Property(e => e.QueryOrderAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("查询订单地址");

                entity.Property(e => e.QuerySignFixedParameter)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("订单查询的固定签名参数");

                entity.Property(e => e.RequestMode)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:form表单-POST请求   2:form表单-GET请求  3:业务POST请求 4:业务GET请求");

                entity.Property(e => e.RequestReturnJavascript)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("签名帮助类");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.SignEndHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("签名后帮助类");

                entity.Property(e => e.SignFixedParameter)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("固定参数");

                entity.Property(e => e.SignHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("签名帮助类");

                entity.Property(e => e.SignName)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("签名参数名称");

                entity.Property(e => e.Status).HasComment("0:禁用 1启用");
            });

            modelBuilder.Entity<OtherPayBankTypeMaps>(entity =>
            {
                entity.ToTable("OtherPayBankTypeMaps", "zdcapitalorder");

                entity.HasComment("第三方银行类型映射");

                entity.HasIndex(e => new { e.BankTypeId, e.OtherPayImpId, e.OtherPayBankTypeCode, e.OtherPayBankTypeName })
                    .HasName("Map_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.BankTypeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("系统银行类型Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.OtherPayBankTypeCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("第三方银行类型编码");

                entity.Property(e => e.OtherPayBankTypeName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("第三方银行类型名称");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("三方Id");

                entity.Property(e => e.PayType).HasColumnType("tinyint(4)");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<OtherPayCurrencyTypeMaps>(entity =>
            {
                entity.ToTable("OtherPayCurrencyTypeMaps", "zdcapitalorder");

                entity.HasComment("第三方数字货币类型映射");

                entity.HasIndex(e => new { e.CurrencyTypeId, e.OtherPayImpId, e.OtherPayCurrencyTypeCode, e.OtherPayCurrencyTypeName })
                    .HasName("Map_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.CurrencyTypeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("系统数字货币类型Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.OtherPayCurrencyTypeCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("第三方数字货币类型编码");

                entity.Property(e => e.OtherPayCurrencyTypeName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasComment("第三方数字货币类型名称");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("三方Id");

                entity.Property(e => e.PayType).HasColumnType("tinyint(4)");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");
            });

            modelBuilder.Entity<OtherPayHelperImp>(entity =>
            {
                entity.ToTable("OtherPayHelperImp", "zdcapitalorder");

                entity.HasComment("第三方支付帮助类实现");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.Assembly)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("程序集");

                entity.Property(e => e.DescriptionContent)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("详细描述");

                entity.Property(e => e.ImpClass)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("实现类");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("名称");

                entity.Property(e => e.Namespace)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("命名空间");

                entity.Property(e => e.PayType).HasColumnType("tinyint(4)");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用1:启用");

                entity.Property(e => e.Type)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:请求参数Build类2:加密类3:签名类4:返回值解析类5:订单编号产生类6:签名后业务处理类7:订单回调8:创建订单");
            });

            modelBuilder.Entity<OtherPayImp>(entity =>
            {
                entity.ToTable("OtherPayImp", "zdcapitalorder");

                entity.HasComment("第三方支付实现");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.AmountFixedParameter)
                    .HasMaxLength(300)
                    .HasDefaultValueSql("''''''")
                    .HasComment("固定金额值 空格隔开");

                entity.Property(e => e.ApplicationScope)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'")
                    .HasComment("应用范围: 0:PC 1:手机H5 2:手机APP 3:所有");

                entity.Property(e => e.CallbackJavascript)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("回调时解析数据的javascript");

                entity.Property(e => e.CreateOrderAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("创建订单地址");

                entity.Property(e => e.CreateOrderHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("创建订单帮助类");

                entity.Property(e => e.CurrencyTypeId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("币种ID");

                entity.Property(e => e.DescriptionContent)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("详细描述");

                entity.Property(e => e.Encoding)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("通道字符编码");

                entity.Property(e => e.EncryptionHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("加密帮助类");

                entity.Property(e => e.InterfaceVersion)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("接口版本");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.IsNotifyCustomerService).HasDefaultValueSql("'0'");

                entity.Property(e => e.IsOpenPostscript)
                    .HasDefaultValueSql("'0'")
                    .HasComment("是否开启附言0:否   1:是");

                entity.Property(e => e.IsRandomAmount)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("随机金额尾数 0:否   1:是");

                entity.Property(e => e.IsShowCallback).HasComment("是否显示给用户callback地址适用于直接在第三方固定回调地址的情况 0:不显示  1:显示");

                entity.Property(e => e.LimitAmountType)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'")
                    .HasComment("0:无限制 1:整10倍数 2:整100倍数 3:整1000倍数 9:手动设置");

                entity.Property(e => e.MatchingTime)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'10'")
                    .HasComment("匹配时间(分钟)");

                entity.Property(e => e.MaxLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最大充值金额");

                entity.Property(e => e.MinLoad)
                    .HasColumnType("int(11)")
                    .HasComment("最小充值金额");

                entity.Property(e => e.Mode)
                    .IsRequired()
                    .HasDefaultValueSql("'1'")
                    .HasComment("0:线上支付1:线下支付");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("名称");

                entity.Property(e => e.OrderCallbackHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单回调帮助类");

                entity.Property(e => e.OrderNoHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("订单号帮助类");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("第三方Id");

                entity.Property(e => e.ParameterType)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("参数类型   String   Json");

                entity.Property(e => e.PayType)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("通道支付方式");

                entity.Property(e => e.PollingType)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'")
                    .HasComment("轮询类型0:银行卡 1:收款码 2:第三方");

                entity.Property(e => e.PostscriptLength)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'0'")
                    .HasComment("附言长度");

                entity.Property(e => e.QueryAmountAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("查询余额地址");

                entity.Property(e => e.QueryMode)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'")
                    .HasComment("查询模式  1:Post  2:Get");

                entity.Property(e => e.QueryOrderAddress)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment("查询订单地址");

                entity.Property(e => e.QuerySignFixedParameter)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("订单查询的固定签名参数");

                entity.Property(e => e.RechargeModeId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("充值模式Id");

                entity.Property(e => e.RequestMode)
                    .HasColumnType("tinyint(4)")
                    .HasComment("1:form表单-POST请求   2:form表单-GET请求  3:业务POST请求 4:业务GET请求");

                entity.Property(e => e.RequestReturnHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("请求返回值帮助类");

                entity.Property(e => e.RequestReturnJavascript)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("业务内部请求订单接口后的javascript");

                entity.Property(e => e.RequirePayName)
                    .HasDefaultValueSql("'0'")
                    .HasComment("线下支付时是否需要填写付款人姓名  0:不需要  1需要");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.ShowCallbackController)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("显示回调地址的控制器 :Id代替MerchantPayInfoId ");

                entity.Property(e => e.SignEndHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("签名后帮助类");

                entity.Property(e => e.SignFixedParameter)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SignHelperId)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasDefaultValueSql("''''''")
                    .HasComment("签名帮助类");

                entity.Property(e => e.SignName)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("签名参数名称");

                entity.Property(e => e.Status).HasComment("0:禁用 1启用");
            });

            modelBuilder.Entity<OtherPayImpRequestParameterMaps>(entity =>
            {
                entity.ToTable("OtherPayImpRequestParameterMaps", "zdcapitalorder");

                entity.HasComment("支付通道请求参数映射");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.OtherPayImpId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付通道ID");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("支付类型Id");

                entity.Property(e => e.ParameterMapTo)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("映射参数");

                entity.Property(e => e.ParameterName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("参数名");

                entity.Property(e => e.PayType).HasColumnType("tinyint(4)");

                entity.Property(e => e.ScopeType)
                    .HasColumnType("tinyint(4)")
                    .HasComment("适用范围 1:请求订单 2:订单查询 3:余额查询");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Value)
                    .HasMaxLength(45)
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("参数值(固定参数时有效)");
            });

            modelBuilder.Entity<OtherPayInfo>(entity =>
            {
                entity.ToTable("OtherPayInfo", "zdcapitalorder");

                entity.HasComment("第三方支付信息");

                entity.HasIndex(e => e.Code)
                    .HasName("Code_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasComment("编码");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasComment("三方名称");

                entity.Property(e => e.PayType)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:充值\\n            1:代付");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:禁用2:启用");
            });

            modelBuilder.Entity<OtherPayRequireParameter>(entity =>
            {
                entity.ToTable("OtherPayRequireParameter", "zdcapitalorder");

                entity.HasComment("第三方支付需求参数");

                entity.HasIndex(e => new { e.ParameterName, e.OtherPayInfoId })
                    .HasName("Data_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.OtherPayInfoId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("第三方Id");

                entity.Property(e => e.ParameterName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("秘钥名称");

                entity.Property(e => e.PayType).HasColumnType("tinyint(4)");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.ShowName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasComment("显示名称");
            });

            modelBuilder.Entity<OtherPayRequireParameterData>(entity =>
            {
                entity.ToTable("OtherPayRequireParameterData", "zdcapitalorder");

                entity.HasComment("商户第三方支付参数数据");

                entity.HasIndex(e => new { e.MerchantId, e.MerchantPayId, e.OtherPayRequireParameterId })
                    .HasName("Data_Unique")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户Id");

                entity.Property(e => e.MerchantPayId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("商户支付配置Id");

                entity.Property(e => e.OtherPayRequireParameterId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.ParameterValue)
                    .IsRequired()
                    .HasComment("秘钥值");
            });

            modelBuilder.Entity<RechargeMode>(entity =>
            {
                entity.ToTable("RechargeMode", "zdcapitalorder");

                entity.HasComment("充值模式");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("充值模式编码");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasComment("充值模式名称");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");

                entity.Property(e => e.Status)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0禁用1启用");

                entity.Property(e => e.Type)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'")
                    .HasComment("类型: 1:线下支付  2:线上支付  3:扫码支付 4:數字貨幣");
            });

            modelBuilder.Entity<RoleAuthorityInfo>(entity =>
            {
                entity.ToTable("RoleAuthorityInfo", "zdcapitalorder");

                entity.HasComment("角色权限关系表");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AuthMark)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("权限标识(Action地址)");

                entity.Property(e => e.AuthorityId)
                    .HasColumnType("int(11)")
                    .HasComment("权限Id");

                entity.Property(e => e.ControllerUrl)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("''''''")
                    .HasComment("模块地址");

                entity.Property(e => e.IsDelete).HasComment("是否删除");

                entity.Property(e => e.ModuleId)
                    .HasColumnType("int(11)")
                    .HasComment("模块Id");

                entity.Property(e => e.RoleId)
                    .HasColumnType("int(11)")
                    .HasComment("角色Id");
            });

            modelBuilder.Entity<RolesInfo>(entity =>
            {
                entity.ToTable("RolesInfo", "zdcapitalorder");

                entity.HasComment("角色信息表");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IsDelete).HasComment("是否删除");

                entity.Property(e => e.MerchantId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("商户Id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasDefaultValueSql("''''''")
                    .HasComment("模块/菜单名称");

                entity.Property(e => e.Remarks)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("''''''")
                    .HasComment("备注");

                entity.Property(e => e.SystemType)
                    .HasColumnType("tinyint(4)")
                    .HasComment("系统类型  0-管理端 1-商户端");
            });

            modelBuilder.Entity<SysConfig>(entity =>
            {
                entity.ToTable("SysConfig", "zdcapitalorder");

                entity.HasComment("系统配置");

                entity.HasIndex(e => e.ItemKey)
                    .HasName("ItemKey");

                entity.HasIndex(e => e.ParentId)
                    .HasName("ParentId");

                entity.Property(e => e.Id)
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("主键Id");

                entity.Property(e => e.DescriptionContent)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("详细描述");

                entity.Property(e => e.IsDelete)
                    .HasColumnType("tinyint(4)")
                    .HasComment("0:未删除1:已删除");

                entity.Property(e => e.ItemKey)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasComment("键");

                entity.Property(e => e.ItemValue)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasComment("值");

                entity.Property(e => e.ParentId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsFixedLength()
                    .HasComment("父级Id");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasComment("排序编号");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
